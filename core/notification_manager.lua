local ruled = require("ruled")

ruled.notification.connect_signal("request::rules", function()
    -- Add a red background for urgent notifications.
    ruled.notification.append_rule {
        rule       = { urgency = "critical" },
        properties = { bg = "#ff0000", fg = "#ffffff", timeout = 0 }
    }

    -- Or green background for normal ones.
    ruled.notification.append_rule {
        rule       = { urgency = "normal" },
        properties = { bg      = "#00ff00", fg = "#000000"}
    }

    ruled.notification.append_rule {
        rule       = { urgency = "low" },
        properties = { bg      = "#00ff00", fg = "#000000"}
    }
end)
