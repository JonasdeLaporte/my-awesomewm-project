--[[
   _____ _ _            _     __  __                                   
  / ____| (_)          | |   |  \/  |                                  
 | |    | |_  ___ _ __ | |_  | \  / | __ _ _ __   __ _  __ _  ___ _ __ 
 | |    | | |/ _ \ '_ \| __| | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
 | |____| | |  __/ | | | |_  | |  | | (_| | | | | (_| | (_| |  __/ |   
  \_____|_|_|\___|_| |_|\__| |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|   
                                                        __/ |          
                                                       |___/           
--]]

local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")
local wl = require("widgets")

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c) 
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end 
end)

--[[
  _____ _ _   _     _                 
 |_   _(_) |_| |___| |__  __ _ _ _ ___
   | | | |  _| / -_) '_ \/ _` | '_(_-<
   |_| |_|\__|_\___|_.__/\__,_|_| /__/
                                      
 Add a titlebar if titlebars_enabled is set to true in the rules. It is
 defined in CONFIG_DIR/widgets/titlebar/titlebar.lua
--]]

client.connect_signal("request::titlebars", function(c)
    setup_titlebar = require(wl.titlebar)
    setup_titlebar(c)
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

--[[
client.connect_signal("property::fullscreen", function(c)
  if c.fullscreen then
	  --naughty.notify({text = "now maximized"})
      awful.titlebar.hide(c)
  else
	  --naughty.notify({text = "not anymore maximized"})
      awful.titlebar.show(c)
  end
end)
--]]

client.connect_signal("focus", function(c) c.border_color = theme.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = theme.border_normal end)
