--[[
   _____                            __  __                                   
  / ____|                          |  \/  |                                  
 | (___   ___ _ __ ___  ___ _ __   | \  / | __ _ _ __   __ _  __ _  ___ _ __ 
  \___ \ / __| '__/ _ \/ _ \ '_ \  | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
  ____) | (__| | |  __/  __/ | | | | |  | | (_| | | | | (_| | (_| |  __/ |   
 |_____/ \___|_|  \___|\___|_| |_| |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|   
                                                              __/ |          
                                                             |___/           
--]]

local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local theme = require("beautiful")

--[[
  _                       _      
 | |   __ _ _  _ ___ _  _| |_ ___
 | |__/ _` | || / _ \ || |  _(_-<
 |____\__,_|\_, \___/\_,_|\__/__/
            |__/                 

 define which layouts are available as fallback if a tag has no own layout
 list configured
--]]

local layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier,
    awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}

awful.layout.append_default_layouts(layouts)

--TODO define a wallpaper(s) function which checks the screen resolution and 
--	   chooses the correct wallpaper
local function set_wallpaper(s)
    -- Wallpaper
    if theme.wallpaper then
        local wallpaper = theme.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end 
end

--[[
  ___     _                       ___                      
 | _ \_ _(_)_ __  __ _ _ _ _  _  / __| __ _ _ ___ ___ _ _  
 |  _/ '_| | '  \/ _` | '_| || | \__ \/ _| '_/ -_) -_) ' \ 
 |_| |_| |_|_|_|_\__,_|_|  \_, | |___/\__|_| \___\___|_||_|
                           |__/                            
--]]

local function setup_primary_screen(s)
    set_wallpaper(s)
    -- "tags::setup_primary" is recieved by CONFIG_DIR/core/tag_manager.lua
    awesome.emit_signal("tags::setup_primary", s)
    -- "panels::setup_primary" is recieved by CONFIG_DIR/core/panel_manager.lua
    awesome.emit_signal("panels::setup_primary", s)
end

--[[
    _      _    _ _ _   _               _   ___                      
   /_\  __| |__| (_) |_(_)___ _ _  __ _| | / __| __ _ _ ___ ___ _ _  
  / _ \/ _` / _` | |  _| / _ \ ' \/ _` | | \__ \/ _| '_/ -_) -_) ' \ 
 /_/ \_\__,_\__,_|_|\__|_\___/_||_\__,_|_| |___/\__|_| \___\___|_||_|
                                                                       
--]]

local function setup_additional_screen(s)
    set_wallpaper(s)
    -- "tags::setup_additional" is recieved by CONFIG_DIR/core/tag_manager.lua
    awesome.emit_signal("tags::setup_additional", s)
    -- "panels::setup_additional" is recieved by CONFIG_DIR/core/panel_manager.lua
    awesome.emit_signal("panels::setup_additional", s)
end

--[[
  ___      _               ___                         
 / __| ___| |_ _  _ _ __  / __| __ _ _ ___ ___ _ _  ___
 \__ \/ -_)  _| || | '_ \ \__ \/ _| '_/ -_) -_) ' \(_-<
 |___/\___|\__|\_,_| .__/ |___/\__|_| \___\___|_||_/__/
                   |_|                                 
--]]

-- define current and future screens
awful.screen.connect_for_each_screen(function(s)
    if s == screen.primary then
        setup_primary_screen(s)
    else
        setup_additional_screen(s)
    end
end)

-- reset wallpaper on geometry changes like different resolution
screen.connect_signal("property::geometry", set_wallpaper)

--[[
screen.connect_signal("scanning", function(s)
    naughty.notify({text = "Recieved Signal SCANNING"})
end)

screen.connect_signal("scanned", function(s)
    naughty.notify({text = "Recieved Signal SCANNED"})
end)

-- xrandr ...
screen.connect_signal("added", function(s)
    naughty.notify({text = "Recieved Signal ADDED"})
end)

-- xrandr ... --off
screen.connect_signal("removed", function(s)
    naughty.notify({text = "Recieved Signal REMOVED"})
end)

screen.connect_signal("list", function(s)
    naughty.notify({text = "Recieved Signal LIST"})
end)

screen.connect_signal("swapped", function(s)
    naughty.notify({text = "Recieved Signal SWAPPED"})
end)

screen.connect_signal("property::viewports", function(s)
    naughty.notify({text = "Recieved Signal PROP::VIEWPORTS"})
end)

screen.connect_signal("request::desktop_decoration", function(s)
    naughty.notify({text = "Recieved Signal REQUEST::DESKTOP_DECORATION"})
end)

screen.connect_signal("request::wallpaper", function(s)
    naughty.notify({text = "Recieved Signal REQUEST::WALLPAPER"})
end)

screen.connect_signal("request::create", function(s)
    naughty.notify({text = "Recieved Signal REQUEST::CREATE"})
end)

screen.connect_signal("request::remove", function(s)
    naughty.notify({text = "Recieved Signal REQUEST::REMOVE"})
end)

screen.connect_signal("request::resize", function(s)
    naughty.notify({text = "Recieved Signal REQUEST::RESIZE"})
end)
--]]
