--[[
  _______            __  __                                   
 |__   __|          |  \/  |                                  
    | | __ _  __ _  | \  / | __ _ _ __   __ _  __ _  ___ _ __ 
    | |/ _` |/ _` | | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
    | | (_| | (_| | | |  | | (_| | | | | (_| | (_| |  __/ |   
    |_|\__,_|\__, | |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|   
              __/ |                            __/ |          
             |___/                            |___/           

 The Tag Manager prepares the tags (=comparable to workspaces). For the 
 detailed configuration of each tag refer to CONFIG_DIR/configuration/tags. 
 The respective signal which will initiate the setup gets emitted by the Screen
 Manager.
--]]

local awful = require("awful")
local cairo = require("lgi").cairo
local gears = require("gears")
local icons = require("theme.icons")
local naughty = require("naughty")
local tl = require("configuration/tags")
local wibox = require("wibox")

local function setup_tags(tags, s)
	for i,tag in pairs(tags) do
		awful.tag.add(tag.name, {
			selected = tag.selected,
			index = i,
			screen = s,
			master_width_factor = tag.master_width_factor,
			layout = tag.layout,
			layouts = tag.layouts,
			volatile = tag.volatile,
			gap = tag.gap,
			gap_single_client = tag.gap_single_client,
			master_fill_policy = tag.master_fill_policy,
			master_count = tag.master_count,
			icon = tag.icon,
			column_count = tag.column_count,
		})
	end
end

-- "tags::setup_primary" gets emitted by CONFIG_DIR/core/screen_manager.lua
awesome.connect_signal("tags::setup_primary", function(s)
	local tags = require(tl.main_tags)
	setup_tags(tags, s)
end)

-- "tags::setup_additional" gets emitted by CONFIG_DIR/core/screen_manager.lua
awesome.connect_signal("tags::setup_additional", function(s)
	local tags = require(tl.basic_tags)
	setup_tags(tags, s)
end)

-- force higher icon quality for client icons requested by "c.icon"
awesome.set_preferred_icon_size(5000) 


--[[
--TODO: trying to convert the client.icon to gray scale icon to only display
--      a colored icon for the currently selected tag
--]]
local conv_to_grey = function(input)
    ---[[
    local output = cairo.ImageSurface.create(
        cairo.Format.ARGB32, 
        input:get_width(),
        input:get_height()
    )
    
    local cr  = cairo.Context(output)
    cr:set_source(gears.color("#ffffff"))
    cr:set_source_surface(input)
--    cr:set_operator(CAIRO_OPERATOR_HSL_LUMINOSITY)
    cr:set_operator(28)
    
    cr:paint()
    --]]
    --print("output: ", output)
    
    return output
end

client.connect_signal("request::activate", function(c) 
        local tag = c.first_tag
        local surface = gears.surface(c.icon)
        --print("icon: ", surface)
        tag.icon = conv_to_grey(surface)
end)

local reset_icon = function(tag)
    if next(tag:clients()) == nil then
        tag.icon = CONFIG_DIR.."configuration/tags/icons/ws"..tag.name.."_w.svg"

    end
end

client.connect_signal("untagged", function(c)
    reset_icon(awful.screen.focused().selected_tag)
end)

    
