--[[
  _____                 _   __  __                                   
 |  __ \               | | |  \/  |                                  
 | |__) |_ _ _ __   ___| | | \  / | __ _ _ __   __ _  __ _  ___ _ __ 
 |  ___/ _` | '_ \ / _ \ | | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
 | |  | (_| | | | |  __/ | | |  | | (_| | | | | (_| | (_| |  __/ |   
 |_|   \__,_|_| |_|\___|_| |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|   
                                                      __/ |          
                                                     |___/           

 The Panel Manager prepares the panels (=bars) so they are ready to be
 displayed on a screen. The PanelManager itself does not spawn any panel; this
 is achieved by registering to a certain signal, which will eventually be
 emitted by the ScreenManager.
--]]

local gears = require("gears")
local naughty = require("naughty")
local wibox = require("wibox")
local wl = require("widgets")
local theme = require("beautiful")
local xresources = require("beautiful.xresources")

local base_panel = require(wl.base_panel)
local dpi = xresources.apply_dpi

--[[
  __  __      _                          _ 
 |  \/  |__ _(_)_ _  _ __  __ _ _ _  ___| |
 | |\/| / _` | | ' \| '_ \/ _` | ' \/ -_) |
 |_|  |_\__,_|_|_||_| .__/\__,_|_||_\___|_|
                    |_|                    

 This panel is the horizontal one on the primary screen. It is created by
 loading the base_panel in CONFIG_DIR/widgets/panels
--]]

local function setup_primary_screen(s)
	local height = s.geometry.height
	local layout = theme.mainpanel_hlayout
    local main_prop = require(wl.main_properties)
    local main_left = require(wl.main_left_widgets)
    local main_middle = require(wl.main_middle_widgets)
    local main_right = require(wl.main_right_widgets)
	local pos = theme.mainpanel_pos
	local width = s.geometry.width

    ---[[
    -- needs to be drawn before the main_panel since the main_bar top spacing
    -- is created by a invisible bar drawn by panel_setup()
    local panel_setup= require("widgets/panels/primary_screen/new_panel")
    local pan = panel_setup(s)
    s.new_panel = pan
    --]]

	if pos == "left" or pos == "right" then
		height = height/3
		layout = theme.mainpanel_vlayout
		-- if the width is too small, the clock widget will look weird
		--TODO find a better solution for this because the clock does
		-- 	   not look good on vertical alignment
		if theme.mainpanel_height < dpi(30) then
			width = dpi(30)
		else
			width = theme.mainpanel_height
		end
		main_prop = main_prop(width, height*3, layout)
	else
        --TODO support for dynamic separator widget
		height = theme.mainpanel_height
		width = 2*width/3
		main_prop = main_prop(width, height, layout)
        width = width/3
	end

    ---[[
    s.main_panel = base_panel(s, main_prop, 
		main_left(s, width, height, layout), 
		main_middle(s, width, height, layout), 
		main_right(s, width, height, layout))
    --]]

end

-- "panels::setup_primary" gets emitted by CONFIG_DIR/core/screen_manager.lua
awesome.connect_signal("panels::setup_primary", function (s)
    setup_primary_screen(s)
end)

--[[
  ___          _                        _ 
 | _ ) __ _ __(_)__ _ __  __ _ _ _  ___| |
 | _ \/ _` (_-< / _| '_ \/ _` | ' \/ -_) |
 |___/\__,_/__/_\__| .__/\__,_|_||_\___|_|
                   |_|                    

 This panel is the horizonzal one on additional screens. It is created by
 loading the base_panel in CONFIG_DIR/widgets/panels
--]]

local function setup_additional_screen(s)
	local height = s.geometry.height
	local layout = theme.basicpanel_hlayout
    local prop = require(wl.basic_properties)
    local left = require(wl.basic_left_widgets)
    local middle = require(wl.basic_middle_widgets)
    local right = require(wl.basic_right_widgets)
	local pos = theme.basicpanel_pos
	local width = s.geometry.width

	if pos == "left" or pos == "right" then
		height = height/3
		layout = theme.basicpanel_vlayout
		-- if the width is too small, the clock widget will look weird
		--TODO find a better solution for this because the clock does
		-- 	   not look good on vertical alignment
        --  -> use wibox.container.rotate
		if theme.basicpanel_height < dpi(30) then
			width = dpi(30)
		else
			width = theme.basicpanel_height
		end
		prop = prop(width, height*3, layout)
	else
		height = theme.mainpanel_height
		width = width/3
		prop = prop(width*3, height, layout)
	end

    s.basic_panel = base_panel(s, prop, 
		left(s, width, height, layout), 
		middle(s, width, height, layout), 
		right(s, width, height, layout))
end

-- "panels::setup_additional" gets emitted by CONFIG_DIR/core/screen_manager.lua
awesome.connect_signal("panels::setup_additional", function (s)
    setup_additional_screen(s)
end)

--[[
-- called when a new (additional) screen is added (e.g startup)
screen.connect_signal("request::desktop_decoration", function(s)
    s.panel = panel(s)
end)
--]]

