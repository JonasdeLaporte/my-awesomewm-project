# My AwesomeWM Project

[AwesomeWM](https://awesomewm.org/) is a window manager - i.e. an alternative to the big, heavyweight desktop environments like KDE Plasma - which I used 
for my Arch Linux distribution. To quote the website of awesomewm: "awesome is a highly configurable, next generation framework window manager for X.
It is very fast, extensible and licensed under the GNU GPLv2 license. It is primarily targeted at power users, developers and any people dealing with
every day computing tasks and who want to have fine-grained control on their graphical environment."

The project is written in Lua and allows to fully program and design everything. Below are some screenshots of the current state.

## Dependencies

additional to the [dependencies](https://awesomewm.org/apidoc/documentation/10-building-and-testing.md.html) of awesome, the additional packages are
required by my custom instance:
- lua53
- lua53-lgi
- system-san-francisco-font-git (AUR)
- rofi
- [compton](https://github.com/PixlOne/compton/tree/dual_kawase) (dual kawase)
- pamixer
- ttf-font-awesome
- conky/conky-manager
- redshift
- nmcli (for the ethernet widget in the main menu)

## Screenshots

![AwesomeWM Screenshot](assets/awesomewm_demo.png)

The wallpaper used in the screenshort was created and is owned by the awesome (!) [fmacmanus](https://www.deviantart.com/fmacmanus/about).
