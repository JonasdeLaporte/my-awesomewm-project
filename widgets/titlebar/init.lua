local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local wibox = require("wibox")
local wl = require("widgets")

local function setup(c)

     -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

	local close_button = require(wl.close_button) 
	local minimize_button = require(wl.minimize_button) 
	local maximize_button = require(wl.maximize_button) 

	local titlebar = awful.titlebar(c, {
		position = "left",
	})

    titlebar : setup {
        { -- Left
            --awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
			--[[
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
			--]]
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
			-- client, top, right, bottom, left
			close_button(c, 5, 5, 3, 5),
			-- client, top, right, bottom, left
			maximize_button(c, 5, 5, 3, 5),
			-- client, top, right, bottom, left
			minimize_button(c, 0, 5, 5, 3),
            layout = wibox.layout.fixed.vertical()
        },
        layout = wibox.layout.align.horizontal
    }
end

return setup
