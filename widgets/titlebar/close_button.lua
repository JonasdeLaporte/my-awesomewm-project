local awful = require("awful")
local wibox = require("wibox")

local function setup(c, top, right, bottom, left)
	local close_button = wibox.widget {
		awful.titlebar.widget.closebutton(c),
		top = top,
		right = right,
		bottom = bottom,
		left = left,
		widget = wibox.container.margin,
	}

	return close_button
end

return setup
