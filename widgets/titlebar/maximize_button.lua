local awful = require("awful")
local wibox = require("wibox")

local function setup(c, top, right, bottom, left)
	local maximize_button = wibox.widget {
		awful.titlebar.widget.maximizedbutton(c),
		top = top,
		right = right,
		bottom = bottom,
		left = left,
		widget = wibox.container.margin,
	}

	return maximize_button
end

return setup
