local awful = require("awful")
local wibox = require("wibox")

local function setup(c, top, right, bottom, left)
	local minimize_button = wibox.widget {
		awful.titlebar.widget.minimizebutton(c),
		top = top,
		right = right,
		bottom = bottom,
		left = left,
		widget = wibox.container.margin,
	}

	return minimize_button
end

return setup
