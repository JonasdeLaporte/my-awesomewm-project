local awful = require("awful")
local theme = require("beautiful")
local wibox = require("wibox")

local layoutbox = function(s)
	local lb = awful.widget.layoutbox(s)
	local height = theme.mainpanel_height
	local margin = wibox.widget {
		lb,
		margins = 3, 
		widget = wibox.container.margin,
	}
	return margin
end

return layoutbox
