local theme = require("beautiful")
local wibox = require("wibox")

local textclock = wibox.widget {
	format = "%H:%M:%S",
	refresh = 1,
	widget = wibox.widget.textclock,
}

local clk = wibox.widget {
	textclock,
	left = 5,
	right = 5,
	top = 3,
	bottom = 3,
	widget = wibox.container.margin,
}

local clock = wibox.widget {
	clk,
	bg = theme.mainclock_bg,
	fg = theme.mainclock_fg,
	shape = theme.mainclock_shape,
	widget = wibox.container.background,
}

local marge = wibox.widget {
    clock,
    top = 3,
    bottom = 3,
    widget = wibox.container.margin,
}

return marge
