local awful = require("awful")
local gears = require("gears")
local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")
local wl = require("widgets")

local image = wibox.widget {
	image = icons.monitor,
	widget = wibox.widget.imagebox,
}

local margin = wibox.widget {
	image,
	top = theme.monitorconf_top_margin,
	bottom = theme.monitorconf_bottom_margin,
	widget = wibox.container.margin,
}

local popup_content = require(wl.monitor_conf_popup)

local popup = awful.popup {
        widget = popup_content(),
        screen = "primary",
	    visible = false,
	    --border_color = "#777777",
	    border_color = "#000000",
        border_width = 5,
		bg = "#00000000",
        fg = "#ffffffff",
        ontop = true,
        placement = awful.placement.centered,
		hide_on_right_click = true,
        shape = gears.shape.rounded_rect,
}

local function toggle_visible()
	local visi = popup.visible
	if not(visi) then
        -- recheck the monitor setup on button click
        popup.widget = popup_content()
	end
	popup.visible = not(visi)
end

-- signal "monitor_conf::toggle_visible" gets emitted by 
-- CONFIG_DIR/widgets/monitor_conf/monitor_conf_popup.lua
awesome.connect_signal("monitor_conf::toggle_visible", function()
    toggle_visible()
end)

image:connect_signal("button::press", function()
    toggle_visible()
end)

return margin
