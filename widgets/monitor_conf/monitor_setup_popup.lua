local awful = require("awful")
local gears = require("gears")
local icons = require("theme/icons")
local naughty = require("naughty")
local wibox = require("wibox")

local font_big = "SF Pro Text Bold 25"
local font_middle = "SF Pro Text Bold 20"
local font_small = "SF Pro Text Bold 12"
local font_color = "FFFFFFFF"

--[[
  ___          _      
 | _ ) ___  __| |_  _ 
 | _ \/ _ \/ _` | || |
 |___/\___/\__,_|\_, |
                 |__/ 
--]]

local function trim_string(str)
    local t = {}
    for substr in string.gmatch(str, "[^%s]+") do
        table.insert(t, substr)
    end 
    return t
end

local function get_available_res(monitor)
    local cmd0 = [[xrandr | sed -n '1, /]]
    local cmd1 = [[/!p' | sed -n '1, /connected/p' | sed -n '/connected/!p' ]]
    local cmd2 = [[| sed 's/\s\{3,\}/#/g' | sed 's/#//' | sed 's/#\+.*//']]
    local cmd = cmd0..monitor..cmd1..cmd2

    local handle = io.popen(cmd)
    local str = handle:read("*a")
    handle:close()

    return trim_string(str)
end

local function get_res_popup(monitor, res_widget)
	local t = get_available_res(monitor)
	local w = {
		spacing = 10,
		layout = wibox.layout.fixed.vertical
	}

	for i,v in pairs(t) do
		local r = wibox.widget {
			text = v,
			font = font_small, 
			widget = wibox.widget.textbox,
		}
		r:connect_signal("button::press", function()
			res_widget.text = v
		end)
		table.insert(w, r)
	end

    local content = wibox.widget {
        w,
        margins = 5,
        widget = wibox.container.margin,
    }

	local p = awful.popup {
    	widget = content,
		visible = false,
		screen = primary,
		ontop = true,
        border_width = 5,
        border_color = "#000000",
		bg = "#00000000",
        fg = "#ffffffff",
		hide_on_right_click = true,
		preferred_positions = "right",
		preferred_anchors = "middle",
		placement = awful.placement.under_mouse,
		shape = gears.shape.rounded_rect,
	}
	return p
end

local function body(id)
	--[[
	  _  _             _ _ _          
     | || |___ __ _ __| | (_)_ _  ___ 
 	 | __ / -_) _` / _` | | | ' \/ -_)
 	 |_||_\___\__,_\__,_|_|_|_||_\___|
                                  
	--]]
	local image = wibox.widget {
		{
			image = icons.back,
			widget = wibox.widget.imagebox,
		},
		margins = 3,
        resize = false,
		forced_width = 40,
	    forced_height = 10,
		widget = wibox.container.margin,
	}
	
	image:connect_signal("button::press", function()
		naughty.notify({text = "close_setup()"})
	end)

    local spacing = wibox.widget {
        forced_width = image.forced_width,
        bg = "#00000000",
        widget = wibox.container.background,
    }

	local headline = wibox.widget {
		{
			text = "Configure "..id,
			font = font_big,
			widget = wibox.widget.textbox,
		},
		halign = "center",
		widget = wibox.container.place,
	}

	--[[
	  ___             _      _   _          
	 | _ \___ ___ ___| |_  _| |_(_)___ _ _  
	 |   / -_|_-</ _ \ | || |  _| / _ \ ' \ 
	 |_|_\___/__/\___/_|\_,_|\__|_\___/_||_|
                                        
	--]]

	local resolution = wibox.widget {
		text = "Select resolution",
		font = font_middle,
		widget = wibox.widget.textbox,
	}

	local res_popup = get_res_popup(id, resolution)
	res_popup:bind_to_widget(resolution)
	
	--[[
	  __  __ _                 
	 |  \/  (_)_ _ _ _ ___ _ _ 
	 | |\/| | | '_| '_/ _ \ '_|
	 |_|  |_|_|_| |_| \___/_|  
                           
	--]]
	
	local mirror = wibox.widget {
		text = "Mirror primary",
		font = font_middle,
		widget = wibox.widget.textbox,
	}

    mirror:connect_signal("button::press", function()
        local res = resolution.text
        if res ~= "Select resolution" then
            naughty.notify({text = "Mirror primary with resolution "..resolution.text})
        else
            naughty.notify({text = "Select resolution first!"})
        end
    end)

	--[[
	  _         __ _        ___   __ 
	 | |   ___ / _| |_ ___ / _ \ / _|
	 | |__/ -_)  _|  _|___| (_) |  _|
	 |____\___|_|  \__|    \___/|_|  
                                 
	--]]
	
	local left = wibox.widget {
		text = "Left of primary",
		font = font_middle, 
		widget = wibox.widget.textbox,
	}

    left:connect_signal("button::press", function()
        local res = resolution.text
        if res ~= "Select resolution" then
            naughty.notify({text = "Set left of primary with resolution "..resolution.text})
        else
            naughty.notify({text = "Select resolution first!"})
        end
    end)

	--[[
	  ___ _      _   _        ___   __ 
	 | _ (_)__ _| |_| |_ ___ / _ \ / _|
	 |   / / _` | ' \  _|___| (_) |  _|
	 |_|_\_\__, |_||_\__|    \___/|_|  
	       |___/                       

	--]]

	local right = wibox.widget {
		text = "Right of primary",
		font = font_middle,
		widget = wibox.widget.textbox,
	}

    right:connect_signal("button::press", function()
        local res = resolution.text
        if res ~= "Select resolution" then
            naughty.notify({text = "Set right of primary with resolution "..resolution.text})
        else
            naughty.notify({text = "Select resolution first!"})
        end
    end)

	--[[
	  _                       _   
	 | |   __ _ _  _ ___ _  _| |_ 
	 | |__/ _` | || / _ \ || |  _|
	 |____\__,_|\_, \___/\_,_|\__|
	            |__/              
	--]]

	local layout = wibox.widget {
		expand = false,
        homogeneous = false, 
		spacing = 20,
		forced_num_cols = 5,
		forced_num_rows = 3,
		layout = wibox.layout.grid,
	}

	layout:add_widget_at(image, 1, 1, 1, 1)
	layout:add_widget_at(headline, 1, 2, 1, 3)
	layout:add_widget_at(spacing, 1, 5, 1, 1)
	layout:add_widget_at({
		resolution,
		halign = "center",
		widget = wibox.container.place,
	}, 2, 2, 1, 3)
	layout:add_widget_at({
		left,
		halign = "center",
		widget = wibox.container.place,
	}, 3, 2, 1, 1)
	layout:add_widget_at({
		mirror,
		halign = "center",
		widget = wibox.container.place,
	}, 3, 3, 1, 1)
	layout:add_widget_at({
		right,
		halign = "center",
		widget = wibox.container.place,
	}, 3, 4, 1, 1)

    local bdy = wibox.widget {
        layout,
        margins = 10,
        widget = wibox.container.margin,
    }

	return bdy
end

--[[
  ___                    
 | _ \___ _ __ _  _ _ __ 
 |  _/ _ \ '_ \ || | '_ \
 |_| \___/ .__/\_,_| .__/
         |_|       |_|   

--]]

local function init_popup(id)
	local popup = awful.popup {
		widget = body(id),
		screen = "primary",
        border_width = 5,
        border_color = "#000000",
		bg = "#00000000",
		fg = "#ffffffff",
		visible = false,
		ontop = true,
		--hide_on_right_click = true,
		placement = awful.placement.centered,
		shape = gears.shape.rounded_rect,
	}
	return popup
end

local function setup(id)
	local popup = init_popup(id)
	popup.visible = not(popup.visible)
end

return setup
