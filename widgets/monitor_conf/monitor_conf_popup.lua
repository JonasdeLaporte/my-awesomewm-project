local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local wibox = require("wibox")
local wl = require("widgets")

--TODO Theme support!!!

local header = wibox.widget {
    id = "header",
    font = "SF Pro Text Bold 25",
    text = "No additional monitors connected...",
    widget = wibox.widget.textbox,
}

local function get_conf_widget(id, i, v)
    local tb = wibox.widget {
        id = "conf_"..i,
        font = "SF Pro Text Bold 20",
        text = "setup",
        widget = wibox.widget.textbox,
    }

    local place = wibox.widget {
        tb,
        halign = "center",
        widget = wibox.container.place,
    }

    local bg = wibox.widget {
        place,
        --bg = "#00000066",
        bg = "#00000000",
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background,
    }
        
    tb:connect_signal("button::press", function()
        -- signal "monitor_conf::toggle_visible" is recived by
        -- CONFIG_DIR/widgets/monitor_conf/monitor_conf.lua
        awesome.emit_signal("monitor_conf::toggle_visible")
        local spawn_setup_popup = require(wl.monitor_setup_popup)
        spawn_setup_popup(v)
    end)

    return bg
end

local function get_monitor_widget(i, v)
    local moni = wibox.widget {
        id = "monitor_"..i,
        font = "SF Pro Text Bold 20",
        text = i..") "..v..": ",
        widget = wibox.widget.textbox,
    }

    local w = wibox.widget {
        moni,
        get_conf_widget("monitor_"..i,i,v),
        homogeneous = true,
        expand = true,
        spacing = 20,
        forced_num_cols = 2,
        forced_num_rows = 1,
        layout = wibox.layout.grid,
    }

    return w
end

local function get_final_widget(t)
    local w = wibox.widget {
        t,
        margins = 25,
        widget = wibox.container.margin,
    }

    return w
end

-- 
local function setup_popup(t)
    local widgets_table = {
        spacing = 10,
        layout = wibox.layout.fixed.vertical
	}
    table.insert(widgets_table, header)

    if next(t) ~= nil then
        header.text = "Connected monitors:"
        for i,v in pairs(t) do
            table.insert(widgets_table, get_monitor_widget(i, v))
        end
    end

    return get_final_widget(widgets_table)
end

-- Modify the given string by cutting whitespaces
-- @param  "str" the string which contains the names of connected monitors
-- @return "t"   a table which provides those names without any newlines
--               or whitespaces
local function trim_string(str)
    local t = {}
    for substr in string.gmatch(str, "[^%s]+") do
        table.insert(t, substr)
    end
    return t
end

-- This is the function which gets called by 
-- CONFIG_DIR/widgets/monitor_conf/monitor_conf.lua to update the content of its popup
-- @return a widgets which can be used without further modification; looks sth like
--     
--          +-----------------+-------+
--          | 1) MONITOR_NAME | setup |
--          +-----------------+-------+
--          :       ...       :  ...  :
--          +-----------------+-------+
--
--         "setup" can be clicked on and will then spawn another popup
local function setup() 
    --TODO exclude the primary monitor by modifying the cmd given to the io.popen() cmd
    local handle = io.popen([[xrandr | sed -n '/disconnected/!p' | grep connected | sed 's/ \+.*//g']])
    local str = handle:read("*a")
    handle:close()
    return setup_popup(trim_string(str))
end

return setup
