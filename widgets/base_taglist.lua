--[[
  _______          _ _     _   
 |__   __|        | (_)   | |  
    | | __ _  __ _| |_ ___| |_ 
    | |/ _` |/ _` | | / __| __|
    | | (_| | (_| | | \__ \ |_ 
    |_|\__,_|\__, |_|_|___/\__|
              __/ |            
             |___/             
--]]

local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")

local base = require("wibox.widget.base")

local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local function setup_taglist (s, prop)
	local template = {
        {
            {
                {
                    id = "icon_role",
                    --font = theme.font,
                    widget = wibox.widget.imagebox,
                },
                id = "marge",
                margins = 10,
                widget = wibox.container.margin,
            },
            halign = "center",
            valign = "center",
            widget = wibox.container.place,
        },
        id = "background_role",
        widget = wibox.container.background,
		create_callback = function(widget, tag, index, objects)
			-- called on creation of a tag
            local marge = widget:get_children_by_id("marge")[1]

			widget:connect_signal("mouse::enter", function()
                marge.margins = 0
			end)

			widget:connect_signal("mouse::leave", function()
                marge.margins = 10 
			end)
		end,
		update_callback = function(widget, tag, index, objects)
		    --called on changes on a existing tag (e.g switching to a existing tag)		
		end,
	}

    local taglist = awful.widget.taglist({
        screen = s,
        filter = prop.filter,
		-- TODO buttons
        buttons = taglist_buttons,
        --forced_height = 540,
        layout = wibox.layout.fixed.vertical,
		widget_template = template,
    })

    local mrgn = wibox.widget {
        taglist,
        margins = 2,
        widget = wibox.container.margin,
    }

    return mrgn
end

return setup_taglist
