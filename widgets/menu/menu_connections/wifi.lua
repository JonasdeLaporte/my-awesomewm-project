local awful = require("awful")
local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")
local wl = require("widgets")

local function dummy_callback()
end

local function setup(interface, width, height)
    local connection = require(wl.connection)
    local eth = connection.setup(interface, "WIFI", width, height)

    local function btn_func(icon)
        if icon.toggled then
            connection.toggle_off(icon)
            awful.spawn.easy_async("nmcli device disconnect "..interface, dummy_callback)
        else
            connection.toggle_on(icon)
            awful.spawn.easy_async("nmcli device connect "..interface, dummy_callback)
        end
    end

    local function menu_func(widget)
        print("This is the menu callback")
        --local cmd = "ip link | grep "..interface.." | sed 's/.* state //g' | sed 's/ .*//'"
        local cmd0 = "nmcli device status | grep "..interface
        local cmd1 = " | sed 's/connected.*/connected/g'  | sed 's/       //g' | sed 's/.*  //'"

        local function callback(output)
            if output == "connected\n" then
                naughty.notify({text = "ethernet is UP"})
                toggle_on(widget)
            elseif output == "disconnected\n" then
                naughty.notify({text = "ethernet is DOWN"})
                toggle_off(widget)
            end

            local cmd = cmd0..cmd1
            print(cmd)
          awful.spawn.easy_async_with_shell (cmd, callback)
        end
    end

    connection.on_button_press(eth, btn_func)
    connection.on_menu_open(eth, menu_func)

    return eth
end

return setup

