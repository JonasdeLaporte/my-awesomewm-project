local gears = require("gears")
local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")
local wl = require("widgets")

local function title(height)
    return wibox.widget {
        {
            -- popup.height/56.8
            font = theme.t_font..""..height/56.8,
            markup = [[<span fgcolor="#FFFFFF"><u>Connections</u></span>]],
            widget = wibox.widget.textbox,
        },
        top = 10,
        bottom = 15,
        left = 25,
        widget = wibox.container.margin,
    }
end

local function setup(parent_height, bg, width, height)
    local ethernet = require(wl.ethernet)
    local wifi = require(wl.wifi)
    --local vpn = require(wl.connection)
    --local bluetooth = require(wl.connection)
    --local disable = require(wl.connection)

    local background = wibox.widget {
        {
            title(parent_height),
            {
                {
                    ethernet("enp0s31f6", 2*width/3, height/6),
                    wifi("wlp3s0", 2*width/3, height/6),
                    --vpn("VPN", 2*width/3, height/6, "test"),
                    --bluetooth("Bluetooth", 2*width/3, height/6, "test"),
                    --disable("Disable all", 2*width/3, height/6, "test"),
                    layout = wibox.layout.fixed.vertical,
                },
                valign = "center",
                halign = "center",
                widget = wibox.container.place,
            },
            layout = wibox.layout.fixed.vertical,
        },
        bg = bg,
        forced_width = width,
        forced_height = height,
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background,
    }

    return background
end

return setup
