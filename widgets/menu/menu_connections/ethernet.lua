local awful = require("awful")
local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")
local wl = require("widgets")

local function dummy_callback()
end

local function setup(interface, width, height)
    local connection = require(wl.connection)
    local eth = connection.setup(interface, "Ethernet", width, height)

    local function btn_func(icon)
        if icon.toggled then
            connection.toggle_off(icon)
            awful.spawn.easy_async("nmcli device disconnect "..interface, dummy_callback)
        else
            connection.toggle_on(icon)
            awful.spawn.easy_async("nmcli device connect "..interface, dummy_callback)
        end
    end

    local function menu_func(widget)

    end

    connection.on_button_press(eth, btn_func)
    connection.on_menu_open(eth, menu_func)

    return eth
end

return setup

