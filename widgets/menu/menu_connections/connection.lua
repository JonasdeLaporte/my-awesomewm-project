local awful = require("awful")
local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")

local connection = {}

function connection.toggle_on(widget)
    widget.image = icons.toggle_on
    widget.toggled = true
end

function connection.toggle_off(widget)
    widget.image = icons.toggle_off
    widget.toggled = false
end

function connection.on_menu_open(widget, callback)
    print(widget)
    print("called connection.on_menu_open()")
    awesome.connect_signal("main_menu::open", callback)
end

function connection.on_button_press(widget, callback)
    local icon = widget:get_children_by_id("icon")[1]
    icon:connect_signal("button::press", callback)
end

function connection.setup(interface, text, width, height)
    local connection = wibox.widget {
        {
            {
                markup = [[<span fgcolor="#FFFFFF">]]..text..[[</span>]],
                font = theme.t_font..height/4,
                forced_width = 3*width/4,
                widget = wibox.widget.textbox,
            },
            {
                {
                    id = "icon",
                    image = icons.toggle_off,
                    toggled = false,
                    widget = wibox.widget.imagebox,
                },
                top = height/5,
                bottom = height/5,
                --left = width/3,
                forced_width = width/4,
                widget = wibox.container.margin,
            },
            layout = wibox.layout.fixed.horizontal,
        },
        forced_height = height,
        forced_width = width,
        valign = "center",
        halign = "left",
        widget = wibox.container.place,
    }

    local debug_bg = wibox.widget {
        connection,
        bg = "#0000ff00",
        widget = wibox.container.background,
    }

    return connection
    --return debug_bg
end

return connection
