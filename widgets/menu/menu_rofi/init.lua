local awful = require("awful")
local cairo = require("lgi").cairo
local gears = require("gears")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")

local function get_rofi_cairo(x, y, width, height)
    local img = cairo.ImageSurface.create(cairo.Format.ARGB32, 100, 300)
    local cr = cairo.Context(img)
    
    cr:set_source(gears.color("#00ff00"))
    cr:rectangle(x, y, width, height)
    cr:fill()
    return img
end

local function execute_callback(input)
    if not input or #input == 0 then return end
    naughty.notification { message = "The input was: >"..input.."<" }
    awful.spawn("rofi -show run -no-config -lines 1 -sidebar-mode", {
        content = get_rofi_cairo(0, 0, 3000, 1000) 
    })
end

local function tb(text, bg)
    return wibox.widget {
        {
            text = text..": ",
            widget = wibox.widget.textbox,
        },
        bg = bg,
        widget = wibox.container.background,
    }
end

local prompt = awful.widget.prompt {
        prompt = "",
        textbox = tb,
        bg = theme.fg_focus.."66",
        bgimage = get_rofi_cairo(0, 0, 50, 10),
        exe_callback = function(input)
            execute_callback(input)
        end,
}

local function setup(text, bg, width, height)
    local background = wibox.widget {
        {
            {
                tb(text, bg),
                prompt,
                layout = wibox.layout.align.horizontal,
            },
            bottom = height/3,
            left = width/20,
            right = width/20,
            top = height/3,
            widget = wibox.container.margin,
        },
        bg = bg,
        forced_width = width,
        forced_height = height,
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background,
    }
    return background
end

prompt:connect_signal("button::press", function() 
    prompt:run()
end)

return setup