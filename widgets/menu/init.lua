local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")
local wl = require("widgets")

local screen = screen.primary
local s_width = screen.geometry.width
local s_height = screen.geometry.height

local popup_width = s_width/6 - 50 - theme.mainpanel_border_width
local popup_height = s_height - theme.mainpanel_height - theme.mainpanel_border_width*2 - 6
local cell_width = popup_width/8
local cell_height = popup_height/16

local num_cols = popup_width/cell_width
local num_rows = popup_height/cell_height


--[[
  ___      _                 ___     _    _ 
 |   \ ___| |__ _  _ __ _   / __|_ _(_)__| |
 | |) / -_) '_ \ || / _` | | (_ | '_| / _` |
 |___/\___|_.__/\_,_\__, |  \___|_| |_\__,_|
                    |___/                   
--]]

local show_grid = false 

local write = io.write

--[[
write("Screen_width: ")
write(s_width.."\n")

write("Popup_width: ")
write(popup_width.."\n\n")

write("Screen_height: ")
write(s_height.."\n")

write("Popup_height: ")
write(popup_height.."\n\n")

write("Cell width: ")
write(cell_width.."\n")

write("Number of columns: ")
write(num_cols.."\n\n")

write("Cell height: ")
write(cell_height.."\n")

write("Number of rows: ")
write(num_rows.."\n\n")
--]]

local function get_filler(row, col)
    local filler = wibox.widget {
        {
            {
                text = show_grid and "Row "..row..",\nCol "..col or "",
                widget = wibox.widget.textbox,
            },
            placement = "center",
            widget = wibox.container.place,
        },
        bg = theme.transparent,
        forced_height = cell_height,
        forced_width = cell_width,
        shape = gears.shape.rectangle,
        shape_border_width = 2,
        shape_border_color = show_grid and "#FFFFFF" or theme.transparent,
        widget = wibox.container.background
    }
    return filler
end

local function fill_grid(grid, m_rows, m_cols)
    --naughty.notify({text = "Filling grid"})

    if m_rows == nil then
        m_rows = 0
    end

    if m_cols == nil then
        m_cols = 0
    end 

    for col=1,m_cols do
        for row=1,m_rows do
            if(not(grid:get_widgets_at(row, col))) then
                --TODO after finishing the menu: create only one filler widget
                local f = get_filler(row, col)
                grid:add_widget_at(f, row, col, 1, 1)
            end
        end
    end
end

--[[
  ___                    
 | _ \___ _ __ _  _ _ __ 
 |  _/ _ \ '_ \ || | '_ \
 |_| \___/ .__/\_,_| .__/
         |_|       |_|   
--]]

local base = wibox.widget {
    {
        id = "layout_grid",
        expand = false,
        forced_num_cols = num_cols,
        forced_num_rows = num_rows,
        min_col_size = cell_width,
        min_rows_size = cell_height,
        spacing = 0,
        layout = wibox.layout.grid,
    },
    --forced_width = s_width/6,
    widget = wibox.container.background,
}
 
local function content()
    local grid = base:get_children_by_id("layout_grid")[1]
    local bg = theme.bg_focus.."88"

    local menu_power = require(wl.menu_power)
    local menu_connections = require(wl.menu_connections)
    local menu_rofi = require(wl.menu_rofi)

    --add('widget', 'row', 'col', 'row_span', 'col_span')
    grid:add_widget_at(menu_power(bg, 4*cell_width, 1*cell_height),2, 3, 1, 4)
    grid:add_widget_at(menu_connections(popup_height, bg, 6*cell_width, 5*cell_height),4, 2, 5, 6)
    grid:add_widget_at(menu_rofi("Rofi", bg, 6*cell_width, 1*cell_height), 15, 2, 1, 6)

    --TODO Warning this obv does take some time and therefore generates a
    --     main loop iteration timer warning
    fill_grid(grid, grid.forced_num_rows, grid.forced_num_cols)

    return base
end

local popup = awful.popup {
    bg = theme.bg_normal.."66",
    border_width = 25,
    border_color = theme.fg_focus.."10",
    --hide_on_right_click = true,
    ontop = true,
    preferred_anchors = "front",
    preferred_positions = "left",
    placement = awful.placement.maximize_vertically,
    screen = screen,
    shape = gears.shape.rounded_rect,
    visible = false,
    widget = content(),
    --x = 0,
    --y = 0,
}

local function toggle_menu(bg)
    if (popup.visible) then
        popup.visible = false
        bg.bg = theme.transparent
        awesome.emit_signal("main_menu::close")
    else
        popup.visible = true
        --bg.bg = "#351543ff"
        bg.bg = theme.fg_focus.."66"
        
        -- signal 'main_menu::open' gets recieved by
        -- widgets/menu/menu_connections/ethernet.lua
        awesome.emit_signal("main_menu::open")
    end
end

return toggle_menu
