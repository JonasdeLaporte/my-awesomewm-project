local gears = require("gears")
local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")

local function get_button(icon, width, height, callback)
    local button = wibox.widget {
        {
            {
                image = icon,
                widget = wibox.widget.imagebox,
            },
            halign = "center",
            valign = "center",
            widget = wibox.container.place,
        },
        forced_width = width,
        forced_height = height,
        top = height/4,
        bottom = height/4,
        widget = wibox.container.margin,
    }

    button:connect_signal("button::press", callback)

    return button
end

local function poweroff()
    naughty.notify({text = "poweroff"})
end

local function reboot()
    naughty.notify({text = "reboot"})
end

local function logout()
    naughty.notify({text = "logout"})
end

local function suspend()
    naughty.notify({text = "suspend"})
end

local function setup(bg, menu_width, menu_height)
    local background = wibox.widget {
        {
            get_button(icons.poweroff, menu_width/4, menu_height, poweroff),
            get_button(icons.reboot, menu_width/4, menu_height, reboot),
            get_button(icons.logout, menu_width/4, menu_height, logout),
            get_button(icons.suspend, menu_width/4, menu_height, suspend),
            layout = wibox.layout.fixed.horizontal,
        },
        bg = bg,
        forced_width = menu_width,
        forced_height = menu_height,
        shape = gears.shape.rounded_rect,
        widget = wibox.container.background,
    }

    return background
end

return setup
