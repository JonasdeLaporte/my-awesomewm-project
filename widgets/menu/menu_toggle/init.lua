local icons = require("theme/icons")
local naughty = require("naughty")
local wibox = require("wibox")
local wl = require("widgets")

local menu = require(wl.menu)

local btn = wibox.widget {
	image = icons.menu_open,
	widget = wibox.widget.imagebox,
}

local mrgn = wibox.widget {
		btn,
		left = 10,
		right = 3,
		top = 4,
		bottom = 4,
		widget = wibox.container.margin,
}

local menu_toggle = wibox.widget {
    mrgn,
    bg = "#00000000",
    widget = wibox.container.background,
}

btn:connect_signal("button::press", function()
	--naughty.notify({text = "open_menu()"})
    menu(menu_toggle)
end)

return menu_toggle
