local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")

local image = wibox.widget {
	image = icons.sun_light,
	widget = wibox.widget.imagebox,
}

local margin = wibox.widget {
	image,
	top = theme.mainthemeswap_top_margin,
	bottom = theme.mainthemeswap_bottom_margin,
	widget = wibox.container.margin,
}

image:connect_signal("button::press", function()
	naughty.notify({text = "swap_theme()"})
end)

return margin
