--[[
 __          ___     _            _       
 \ \        / (_)   | |          | |      
  \ \  /\  / / _  __| | __ _  ___| |_ ___ 
   \ \/  \/ / | |/ _` |/ _` |/ _ \ __/ __|
    \  /\  /  | | (_| | (_| |  __/ |_\__ \
     \/  \/   |_|\__,_|\__, |\___|\__|___/
                        __/ |             
                       |___/              

 This file provides a list of all available widgets
--]]

local WIDGETS = "widgets/"

local BASE_PANEL = WIDGETS.."panels/additional_screen/basic_panel/basic_"
local MAIN_PANEL = WIDGETS.."panels/primary_screen/main_panel/main_"

local widget_list = {
    base_taglist = WIDGETS.."base_taglist",
    base_panel = WIDGETS.."panels/base_panel",
    -- primary screen
    main_left_widgets = MAIN_PANEL.."left_widgets",
    main_middle_widgets = MAIN_PANEL.."middle_widgets",
    main_properties = MAIN_PANEL.."properties",
    main_right_widgets = MAIN_PANEL.."right_widgets",
    -- additional screen
    basic_left_widgets = BASE_PANEL.."left_widgets",
    basic_middle_widgets = BASE_PANEL.."middle_widgets",
    basic_properties = BASE_PANEL.."properties",
    basic_right_widgets = BASE_PANEL.."right_widgets",
	--
	separator = WIDGETS.."panels/separator",
    menu = WIDGETS.."menu",
	menu_toggle = WIDGETS.."menu/menu_toggle",
    menu_power = WIDGETS.."menu/menu_power",
    menu_rofi = WIDGETS.."menu/menu_rofi",
    menu_connections = WIDGETS.."menu/menu_connections",
    connection = WIDGETS.."menu/menu_connections/connection",
    ethernet = WIDGETS.."menu/menu_connections/ethernet",
    wifi = WIDGETS.."menu/menu_connections/wifi",
    vpn = WIDGETS.."menu/menu_connections/vpn",
    bluetooth = WIDGETS.."menu/menu_connections/bluetooth",
    disable_all_connections = WIDGETS.."menu/menu_connections/disable_all",
	layoutbox = WIDGETS.."layoutbox",
	clock = WIDGETS.."clock",
	theme_swap = WIDGETS.."theme_swap/theme_swap",
	keymap = WIDGETS.."keymap",
	tray_toggle = WIDGETS.."tray_toggle/tray_toggle",
	monitor_conf = WIDGETS.."monitor_conf",
	monitor_conf_popup = WIDGETS.."monitor_conf/monitor_conf_popup",
    monitor_setup_popup = WIDGETS.."monitor_conf/monitor_setup_popup",
    titlebar = WIDGETS.."titlebar",
	close_button = WIDGETS.."titlebar/close_button",
	minimize_button = WIDGETS.."titlebar/minimize_button",
	maximize_button = WIDGETS.."titlebar/maximize_button",
}

return widget_list
