local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")

local image = wibox.widget {
	image = icons.keyboard,
	widget = wibox.widget.imagebox,
}

local margin = wibox.widget {
	image,
	--margins = 3,
	top = theme.mainkeymap_top_margin,
	bottom = theme.mainkeymap_bottom_margin,
	widget = wibox.container.margin,
}

image:connect_signal("button::press", function()
	naughty.notify({text = "switch_keymap()"})
end)

return margin
