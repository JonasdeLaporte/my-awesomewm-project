local icons = require("theme.icons")
local theme = require("beautiful")
local wibox = require("wibox")

local setup_tray_toggle = function(panel_layout)
	local image = wibox.widget {
		image = icons.tray_closed,
	    widget = wibox.widget.imagebox,
	}

	local trays = wibox.widget {
		{
			id = "systrays",
			visible = false,
			widget = wibox.widget.systray,
		},
		margins = 3,
		widget = wibox.container.margin,
	}

	local tray_toggle = wibox.widget {
		{
			trays,
			{
				image,
				top = 4,
				bottom = 4,
				widget = wibox.container.margin,
			},
			layout = panel_layout,
		},
		bg = theme.transparent,
		fg = theme.mainpanel_fg,
		widget = wibox.container.background,
	}

	tray_toggle:connect_signal("button::press", function()
		if trays.systrays.visible then
			image.image = icons.tray_closed
		else
			image.image = icons.tray_opened
		end
		trays.systrays.visible = not(trays.systrays.visible)
	end)
	return tray_toggle
end

return setup_tray_toggle
