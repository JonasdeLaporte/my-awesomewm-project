local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")
local wibox = require("wibox")
local wl = require("widgets")

local function setup(s)
    local tl = require(wl.base_taglist)

    local mytaglist = tl(s, {
        --filter  = awful.widget.taglist.filter.noempty,
        filter  = awful.widget.taglist.filter.all,
        layout = wibox.layout.fixed.vertical,
    })

    local spacer =  function()
        return awful.wibar {
            screen = s,
            stretch = true,
            bg = "#0000ff00",
            --height = 40,
            width = 70,
            position = "left",
            type = "utility"
        }
    end

    local s_geo = s.geometry

    local observer = wibox({
        bg = "#ff000000",
        width = 60,
        height = 618,
        --height = 1440,
        screen = s,
        shape = gears.shape.rounded_rect,
        x = s_geo.x + 10,
        y = (s_geo.height - 540)/2,
        --y = 0,
        visible = true,
        ontop = true,
        type = "utility",
    })

    local left_panel = wibox({
        --bg = "#414141ff",
        bg = "#41414166",
        width = observer.width,
        height = observer.height,
        screen = observer.screen,
        shape = observer.shape,
        x = observer.x,
        y = observer.y,
        visible = true,
        ontop = true,
        widget = mytaglist,
        border_width = 2,
        border_color = "#00000000",
        --toggled = false,
    })

    --naughty.notify({text = "geometry: "..s.geometry.x})

    left_panel.toggled = true
    left_panel.spacer = spacer()

    left_panel:connect_signal("button::release", function(self, x, y, button, mods)
        if (button == 1 and mods[1] == "Shift") then
            --naughty.notify({text = "button pressed"})

            left_panel.toggled = not(left_panel.toggled)

            if (left_panel.toggled) then
                left_panel.spacer = spacer()
            else
                left_panel.spacer:remove()
            end
        end
    end)

    local toggle_panel_visibility = function()    
        left_panel.visible = true
    end

    left_panel:connect_signal("mouse::enter", toggle_panel_visibility)
    observer:connect_signal("mouse::enter", toggle_panel_visibility)

    left_panel:connect_signal("mouse::leave", function()
        --naughty.notify({text = "I left!"})
        left_panel.visible = left_panel.toggled
    end)

    -- TODO relocate this into the mainbar file
    local main_spacer = awful.wibar {
        position = "top",
        type = "utility",
        bg = "#ff000000",
        height = "10",
    }

    return left_panel

end

return setup
