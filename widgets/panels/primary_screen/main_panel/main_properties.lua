local gears = require("gears")
local theme = require("beautiful")
local wibox = require("wibox")

local props = function(width, height, panel_layout)
	return {
    	bg = theme.mainpanel_bg,
        border_color = theme.mainpanel_border_color,
        border_width = theme.mainpanel_border_width,
    	height = height, 
    	layout = panel_layout,
    	ontop = theme.mainpanel_ontop, 
    	position = theme.mainpanel_pos, 
    	shape = theme.mainpanel_shape, 
        type = "dock",
		width = width,
	}
end

return props
