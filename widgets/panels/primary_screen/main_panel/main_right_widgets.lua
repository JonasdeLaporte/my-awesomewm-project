local awful = require("awful")
local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")

local function setup_right(s, width, height, panel_layout)
	local wl = require("widgets")

	-- === Load widgets here: === --
	local keymap = require(wl.keymap)
	local layoutbox = require(wl.layoutbox)	
	local monitor_conf = require(wl.monitor_conf)
	local separator = require(wl.separator)
	local theme_swap = require(wl.theme_swap)
	local tray_toggle = require(wl.tray_toggle)
	-- ========================== --
	
    local right = {
        layout = panel_layout,
		{
            {
                {
                    layout = panel_layout,
                    -- ===  Add widgets here: === --
                    tray_toggle(panel_layout),
                    separator,
                    theme_swap,
                    separator,
                    keymap,
                    separator,
                    monitor_conf,
                    separator,
                    layoutbox(s),
                    -- ========================== --
                },
                forced_width = width,
                forced_height = height,
                valign = "bottom",
                halign = "right",
                widget = wibox.container.place
            },
            top = 0,
            bottom = 0,
            right = 10,
            widget = wibox.container.margin,

    	}
	}
    return right
end

return setup_right
