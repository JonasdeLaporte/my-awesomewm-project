local awful = require("awful")
local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")

local function setup_middle(s, width, height, panel_layout)
    --TODO remove local widget declaration by calling setup functions of
	
	local wl = require("widgets")

	-- === Load widgets here: === --
	local clock = require(wl.clock)
	-- ========================== --

    local middle = {
		fill_space = false,
        layout = panel_layout,
		{
			{
				layout = panel_layout,
				-- ===  Add widgets here: === --
				clock,
				-- ========================== --
			},
			forced_width = width,
			forced_height = height,
			valign = "center",
			halign = "center",
			widget = wibox.container.place
    	}
	}
    return middle
end

return setup_middle
