local awful = require("awful")
local icons = require("theme/icons")
local wibox = require("wibox")

local function setup_left(s, width, height, panel_layout)
	
	local wl = require("widgets")

	-- === Load widgets here: === --
	local menu_open = require(wl.menu_toggle)
    --local taglist = require(wl.base_taglist)
	-- ========================== --

    --[[
    local mytaglist = taglist(s, {
        --filter  = awful.widget.taglist.filter.noempty,
        filter  = awful.widget.taglist.filter.all,
        layout = panel_layout,
    })
    --]]

    local left = {
		{
			{
				-- ===  Add widgets here: === --
				menu_open,
        		--mytaglist,
				-- ========================== --
				spacing = 10,
				layout = panel_layout,
			},
			forced_width = width,
			forced_height = height,
			valign = "top",
			halign = "left",
			widget = wibox.container.place
    	},
        layout = panel_layout,
	}
    return left
end

return setup_left
