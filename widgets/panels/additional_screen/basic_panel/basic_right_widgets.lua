local awful = require("awful")
local icons = require("theme/icons")
local naughty = require("naughty")
local theme = require("beautiful")
local wibox = require("wibox")

local function setup_right(s, width, height, panel_layout)
	local wl = require("widgets")

	-- === Load widgets here: === --
	local layoutbox = require(wl.layoutbox)	
	-- ========================== --

    local right = {
        layout = panel_layout,
		{
			{
				layout = panel_layout,
				-- ===  Add widgets here: === --
				layoutbox(s),
				-- ========================== --
			},
			forced_width = width,
			forced_height = height,
			valign = "bottom",
			halign = "right",
			widget = wibox.container.place
    	}
	}
    return right
end

return setup_right
