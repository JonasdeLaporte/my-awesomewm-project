local awful = require("awful")
local icons = require("theme/icons")
local wibox = require("wibox")

local function setup_left(s, width, height, panel_layout)
    --TODO remove local widget declaration by calling setup functions of
    --     externally generic widgets in /CONFIG_DIR/widgets
	
	local wl = require("widgets")

	-- === Load widgets here: === --
	-- ========================== --

    local left = {
		{
			{
				-- ===  Add widgets here: === --
				-- ========================== --
				spacing = 10,
				layout = panel_layout,
			},
			forced_width = width,
			forced_height = height,
			valign = "top",
			halign = "left",
			widget = wibox.container.place
    	},
        layout = panel_layout,
	}
    return left
end

return setup_left
