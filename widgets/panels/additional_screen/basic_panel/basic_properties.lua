local gears = require("gears")
local theme = require("beautiful")
local wibox = require("wibox")

local props = function(width, height, panel_layout)
	return {
    	bg = theme.basicpanel_bg,
    	height = height, 
    	layout = panel_layout,
    	ontop = theme.basicpanel_ontop, 
    	position = theme.basicpanel_pos, 
    	shape = theme.basicpanel_shape, 
        type = "utility",
		width = width,
	}
end

return props
