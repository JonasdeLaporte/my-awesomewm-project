--[[
  ____                   _____                 _ 
 |  _ \                 |  __ \               | |
 | |_) | __ _ ___  ___  | |__) |_ _ _ __   ___| |
 |  _ < / _` / __|/ _ \ |  ___/ _` | '_ \ / _ \ |
 | |_) | (_| \__ \  __/ | |  | (_| | | | |  __/ |
 |____/ \__,_|___/\___| |_|   \__,_|_| |_|\___|_|
                                                 
 This is the foundation of every panel (=bar) on the different screens. It is
 possible to call the panel with a table "prop" which holds the different
 desiered properties. If no table is given, the default values are used.
 The parameters "left", "middle" and "right" are mandatory tables. They
 provide the widgets which will be shown on the panel and have to contain at 
 least one line defining the desiered layout like so:
    e.g layout = wibox.layout.fixed.horizontal
 if this line is not provided, awesomewm will crash with a error message
--]]

local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")

local panel = function(s, prop, left, middle, right)
	local mb = awful.wibar {
		screen = s or "primary",
		position = prop.position or "top",
		height = prop.height or 20,
		width = prop.width or nil,
		ontop = prop.ontop or false,
		shape = prop.shape or gears.shape.rectangle,
		bg = prop.bg or "#000000",
		border_width = prop.border_width or 0,
		border_color = prop.border_color or "#00000000",
        type = prop.type,
	}

	mb:setup {
		layout = prop.layout 
            or wibox.layout.align.horizontal,
        left,
        middle,
        right,
	}

	return mb
end

return panel
