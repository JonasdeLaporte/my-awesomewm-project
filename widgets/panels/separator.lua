local theme = require("beautiful")
local wibox = require("wibox")

--TODO add support for "left" and "right" panel positions
local separator = wibox.widget {
    {   
        orientation = theme.mainseparator_orientation,
        thickness = theme.mainseparator_thickness,
        color = theme.mainseparator_color,
        forced_width = theme.mainseparator_forced_width, 
        --forced_height = theme.mainpanel_height - 10,
        widget = wibox.widget.separator
    },  
    top = theme.mainseparator_top_margin,
    bottom = theme.mainseparator_bottom_margin,
    widget = wibox.container.margin,
}

return separator
