--[[
  _____       _           
 |  __ \     | |          
 | |__) |   _| | ___  ___ 
 |  _  / | | | |/ _ \/ __|
 | | \ \ |_| | |  __/\__ \
 |_|  \_\__,_|_|\___||___/
                          
 this file assigns properties to clients like keybindings, titlebar and the
 respective attributes which were defined in the CONFIG_DIR/themes/theme file
--]]

local awful = require("awful")
local theme = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")

-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = theme.border_width,
                     border_color = theme.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
					 -- clientkeys and clientbuttons are global vars which were
					 -- initialized by CONFIG_DIR/configuration/keys
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+
                       awful.placement.no_offscreen+
                       awful.placement.centered,
                     shape = theme.client_shape,
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer",
          "java-lang-Thread", -- the initial window of IntelliJ
        },

        -- Note that the name property shown in xprop might be set slightly 
		-- after creation of the client and the name shown there might not 
		-- match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { 
        	placement = awful.placement.centered,
			floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = true }
    },
    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
    { rule = { class = "steam_app_.*" },
      properties = { screen = "primary", tag = "0" } },
	--[[
    { rule = { class = {"TelegramDesktop", "Eclipse"}},
      callback = function(c) 
          c:connect_signal("unfocus", function(c)
            client.connect_signal("focus", function(cli)
                if cli.name == "Media viewer" then
                    cli:connect_signal("unfocus", function()
                        c:activate()
                    end)
                end
            end)
          end)
      end},
	  --]]
    { rule_any = { class = {"Pystopwatch", "llpp", "Pavucontrol", "obs"} },
      properties = {ontop = true}},
}
