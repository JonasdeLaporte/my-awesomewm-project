--[[
  __  __       _         _______              
 |  \/  |     (_)       |__   __|             
 | \  / | __ _ _ _ __      | | __ _  __ _ ___ 
 | |\/| |/ _` | | '_ \     | |/ _` |/ _` / __|
 | |  | | (_| | | | | |    | | (_| | (_| \__ \
 |_|  |_|\__,_|_|_| |_|    |_|\__,_|\__, |___/
                                     __/ |    
                                    |___/     

 This file contains the properties of the tags which will be used for the
 primary screen.
--]]

local awful = require("awful")
local icons = require("theme.icons")
local l = awful.layout.suit
local theme = require("beautiful")

local tags = {
	{
        name = "1",
		selected = true,
		master_width_factor = 0.5,
        layout = l.floating,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws1,
		column_count = 2,
    },
	{
        name = "2",
		selected = false,
		master_width_factor = 0.5,
        layout = l.magnifier,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws2,
		column_count = 2,
    },
	{
        name = "3",
		selected = false,
		master_width_factor = 0.9,
        layout = l.fair,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws3,
		column_count = 2,
    },
	{
        name = "4",
		selected = false,
		master_width_factor = 0.5,
        layout = l.fair,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws4,
		column_count = 2,
    },
	{
        name = "5",
		selected = false,
		master_width_factor = 0.5,
        layout = l.fair,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws5,
		column_count = 2,
    },
	{
        name = "6",
		selected = false,
		master_width_factor = 0.5,
        layout = l.fair,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws6,
		column_count = 2,
    },
	{
        name = "7",
		selected = false,
		master_width_factor = 0.5,
        layout = l.fair,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws7,
		column_count = 2,
    },
	{
        name = "8",
		selected = false,
		master_width_factor = 0.5,
        layout = l.fair,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws8,
		column_count = 2,
    },
	{
        name = "9",
		selected = false,
		master_width_factor = 0.5,
        layout = l.fair,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws9,
		column_count = 2,
    },
	{
        name = "0",
		selected = false,
		master_width_factor = 0.5,
        layout = l.magnifier,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = theme.useless_gap,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = icons.ws0,
		column_count = 2,
    },
}

return tags
