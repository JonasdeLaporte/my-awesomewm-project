local TAGS = "configuration/tags/"

return {
	main_tags = TAGS.."main_tags",
	basic_tags = TAGS.."basic_tags",
}
