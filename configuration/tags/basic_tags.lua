--[[
  ____            _        _______              
 |  _ \          (_)      |__   __|             
 | |_) | __ _ ___ _  ___     | | __ _  __ _ ___ 
 |  _ < / _` / __| |/ __|    | |/ _` |/ _` / __|
 | |_) | (_| \__ \ | (__     | | (_| | (_| \__ \
 |____/ \__,_|___/_|\___|    |_|\__,_|\__, |___/
                                       __/ |    
                                      |___/     

 This file contains the properties of the tags which will be used for
 additional screen.
--]]

local awful = require("awful")
local icons = require("theme.icons")
local l = awful.layout.suit

local tags = {
	{
        name = "1",
		selected = true,
		master_width_factor = 0.5,
        layout = l.magnifier,
		layouts = {
			l.floating,
			l.fair,
			l.max,
			l.magnifier,
		},
		volatile = false,
		gap = 5,
		gap_single_client = true,
		master_fill_policy = "expand",
		master_count = 2,
		icon = nil,
		column_count = 2,
    },
}

return tags
