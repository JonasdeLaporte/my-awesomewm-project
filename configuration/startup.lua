--[[
   _____ _             _               
  / ____| |           | |              
 | (___ | |_ __ _ _ __| |_ _   _ _ __  
  \___ \| __/ _` | '__| __| | | | '_ \ 
  ____) | || (_| | |  | |_| |_| | |_) |
 |_____/ \__\__,_|_|   \__|\__,_| .__/ 
                                | |    
                                |_|    
--]]

local awful = require("awful")
local theme = require("beautiful")

local function startup(cmd)
    awful.spawn.easy_async(cmd, function() end)
end

startup("compton --blur-method kawase --blur-strength "..theme.blur_strength.." --config "..CONFIG_DIR.."configuration/compton/compton.conf")
startup("flameshot")
startup("nm-applet")
--startup("xrandr --setprovideroutputsource NVIDIA-G0 Intel")
