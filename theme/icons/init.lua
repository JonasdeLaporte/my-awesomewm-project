local gears = require("gears")
local dir = CONFIG_DIR.."widgets/"
local tags = CONFIG_DIR.."configuration/tags/icons/"

return {
	--Mainbar
	menu_open = dir.."menu/menu_toggle/menu_open.svg",
	sun_light = dir.."theme_swap/sun_light.svg",
	keyboard = dir.."keymap/keyboard.svg",
	--log = dir.."sys_log/log.svg",
	tray_closed = dir.."tray_toggle/tray_closed.svg",
	tray_opened = dir.."tray_toggle/tray_opened.svg",
	monitor = dir.."monitor_conf/monitor.svg",
    back = dir.."monitor_conf/back.svg",
	--Titlebar
	close = dir.."titlebar/close_1.svg",
	min = dir.."titlebar/minimize_1.svg",
	max = dir.."titlebar/maximize_1.svg",
    --Mainmenu
    poweroff = dir.."menu/menu_power/poweroff.svg",
    reboot = dir.."menu/menu_power/reboot.svg",
    logout = dir.."menu/menu_power/logout.svg",
    suspend = dir.."menu/menu_power/suspend.svg",
    --
    toggle_on = dir.."menu/menu_connections/toggle_on.svg",
    toggle_off = dir.."menu/menu_connections/toggle_off.svg",
    -- Taglist
    ws1 = tags.."ws1_w.svg",
    ws2 = tags.."ws2_w.svg",
    ws3 = tags.."ws3_w.svg",
    ws4 = tags.."ws4_w.svg",
    ws5 = tags.."ws5_w.svg",
    ws6 = tags.."ws6_w.svg",
    ws7 = tags.."ws7_w.svg",
    ws8 = tags.."ws8_w.svg",
    ws9 = tags.."ws9_w.svg",
    ws0 = tags.."ws0_w.svg",
     
}
