--[[
_________          _______  _______  _______ 
\__   __/|\     /|(  ____ \(       )(  ____ \
   ) (   | )   ( || (    \/| () () || (    \/
   | |   | (___) || (__    | || || || (__    
   | |   |  ___  ||  __)   | |(_)| ||  __)   
   | |   | (   ) || (      | |   | || (      
   | |   | )   ( || (____/\| )   ( || (____/\
   )_(   |/     \|(_______/|/     \|(_______/
                                             
 This is the main configuration file of awesomewm; every parameter that is
 related to the graphic surface, can be found and modified here.
 For logic configuration like keybindigs and client rules, refere to the files
 in /CONFIG_DIR/configuration. 
 TODO maybe those configuration will be added
 here later by defining an additional set of global theme-parameters which 
 will then be called by the respective file
 TODO add multiple theme.lua files to quickly swap between light and dark
 modes and a season specific base theme
--]]

local gears = require("gears")
local gfs = require("gears.filesystem")
local icons = require("theme/icons")
local theme_assets = require("beautiful.theme_assets")
local themes_path = gfs.get_themes_dir()
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local wibox = require("wibox")

local theme = {}

local pri = "#1a354b"
local sec = "#251a4b"
local ter = "#4b2c1a"

--[[
  ____           _____ ______ 
 |  _ \   /\    / ____|  ____|
 | |_) | /  \  | (___ | |__   
 |  _ < / /\ \  \___ \|  __|  
 | |_) / ____ \ ____) | |____ 
 |____/_/    \_\_____/|______|
                              
--]]

theme.font          = "SF Pro Text Bold 10"
theme.t_font        = "SF Pro Text Bold "
theme.transparent   = "#00000000"

theme.bg_normal     = pri
theme.bg_focus      = ter
theme.bg_urgent     = "#b34700"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = sec 
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"


-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"
theme.hotkeys_bg = theme.bg_normal.."aa"


--[[
 __          __   _ _                             
 \ \        / /  | | |                            
  \ \  /\  / /_ _| | |_ __   __ _ _ __   ___ _ __ 
   \ \/  \/ / _` | | | '_ \ / _` | '_ \ / _ \ '__|
    \  /\  / (_| | | | |_) | (_| | |_) |  __/ |   
     \/  \/ \__,_|_|_| .__/ \__,_| .__/ \___|_|   
                     | |         | |              
                     |_|         |_|              
--]]

--theme.wallpaper = CONFIG_DIR.."theme/wallpapers/wallpaper_dark-souls.jpg"
theme.wallpaper = CONFIG_DIR.."theme/wallpapers/3440x1440_18.png"

--[[
theme.wallpaper = function(s)
	os.execute("feh --bg-fill ~/wallpapers/wallpaper_color-mix_02.jpg")
end
--]]

--[[
   _____                      _              
  / ____|                    | |             
 | |     ___  _ __ ___  _ __ | |_ ___  _ __  
 | |    / _ \| '_ ` _ \| '_ \| __/ _ \| '_ \ 
 | |___| (_) | | | | | | |_) | || (_) | | | |
  \_____\___/|_| |_| |_| .__/ \__\___/|_| |_|
                       | |                   
                       |_|                   
--]]

theme.blur_strength = 20

--[[
  _____      _                               _____                          
 |  __ \    (_)                             / ____|                         
 | |__) | __ _ _ __ ___   __ _ _ __ _   _  | (___   ___ _ __ ___  ___ _ __  
 |  ___/ '__| | '_ ` _ \ / _` | '__| | | |  \___ \ / __| '__/ _ \/ _ \ '_ \ 
 | |   | |  | | | | | | | (_| | |  | |_| |  ____) | (__| | |  __/  __/ | | |
 |_|   |_|  |_|_| |_| |_|\__,_|_|   \__, | |_____/ \___|_|  \___|\___|_| |_|
                                     __/ |                                  
                                    |___/                                   
  __  __      _        ___               _ 
 |  \/  |__ _(_)_ _   | _ \__ _ _ _  ___| |
 | |\/| / _` | | ' \  |  _/ _` | ' \/ -_) |
 |_|  |_\__,_|_|_||_| |_| \__,_|_||_\___|_|
                                           
--]]

theme.mainpanel_bg = "#41414199" 
theme.mainpanel_fg = theme.fg_normal.."ff"
--TODO generic height
theme.mainpanel_height = 25
theme.mainpanel_ontop = false
theme.mainpanel_shape = gears.shape.rounded_rect
theme.mainpanel_border_color = theme.transparent

theme.mainpanel_border_width = 2

--TODO "left" and "right" are not fully supported yet
theme.mainpanel_pos = "top"
theme.mainpanel_hlayout = wibox.layout.fixed.horizontal
theme.mainpanel_vlayout = wibox.layout.fixed.vertical

--[[
  __  __      _     _             _ _    _   
 |  \/  |__ _(_)_ _| |_ __ _ __ _| (_)__| |_ 
 | |\/| / _` | | ' \  _/ _` / _` | | (_-<  _|
 |_|  |_\__,_|_|_||_\__\__,_\__, |_|_/__/\__|
                            |___/            
--]]

--The tag list main foreground (text) color
--theme.taglist_fg_focus = theme.fg_focus.."ff"
theme.taglist_fg_focus = theme.transparent

--The tag list main background color
--theme.taglist_bg_focus = theme.bg_focus.."ff"
theme.taglist_bg_focus = "#ffffff44"

--The tag list urgent elements foreground (text) color
theme.taglist_fg_urgent = theme.fg_urgent.."ff"

--The tag list urgent elements background color
theme.taglist_bg_urgent = theme.bg_urgent.."ff"

--The tag list occupied elements background color
--theme.taglist_bg_occupied = theme.bg_normal.."ff"
theme.taglist_bg_occupied = theme.transparent

--The tag list occupied elements foreground (text) color
--theme.taglist_fg_occupied = theme.fg_focus.."ff"
theme.taglist_fg_occupied = theme.transparent

--The tag list empty elements background color
theme.taglist_bg_empty = theme.transparent
--theme.taglist_bg_empty = theme.bg_normal.."ff"

--The tag list empty elements foreground (text) color
--theme.taglist_fg_empty = theme.fg_normal.."ff"
theme.taglist_fg_empty = theme.transparent

--The tag list volatile elements background color
--theme.taglist_bg_volatile =

--The tag list volatile elements foreground (text) color
--theme.taglist_fg_volatile = theme.fg_normal.."ff"

--The selected elements background image
--theme.taglist_squares_sel =

--The unselected elements background image
--theme.taglist_squares_unsel =

--The selected empty elements background image
--theme.taglist_squares_sel_empty =

--The unselected empty elements background image
--theme.taglist_squares_unsel_empty =

--If the background images can be resized
--theme.taglist_squares_resize =

--Do not display the tag icons, even if they are set
--theme.taglist_disable_icon =

--The taglist font
theme.taglist_font = theme.font

--The space between the taglist elements
theme.taglist_spacing = dpi(6)

--The main shape used for the elements
theme.taglist_shape = gears.shape.rounded_rect
--theme.taglist_shape = gears.shape.circle

--The shape elements border width
--theme.taglist_shape_border_width = dpi(1)

--The elements shape border color
--theme.taglist_shape_border_color = theme.fg_normal.."ff"

--The shape used for the empty elements
theme.taglist_shape_empty = theme.taglist_shape

--The shape used for the empty elements border width
theme.taglist_shape_border_width_empty = theme.taglist_shape_border_width

--The empty elements shape border color
theme.taglist_shape_border_color_empty = theme.taglist_shape_border_color

--The shape used for the selected elements
theme.taglist_shape_focus = theme.taglist_shape

--The shape used for the selected elements border width
theme.taglist_shape_border_width_focus = theme.taglist_shape_border_width

--The selected elements shape border color
theme.taglist_shape_border_color_focus = theme.fg_focus

--The shape used for the urgent elements
--theme.taglist_shape_urgent =

--The shape used for the urgent elements border width
--theme.taglist_shape_border_width_urgent =

--The urgents elements shape border color
--theme.taglist_shape_border_color_urgent =

--The shape used for the volatile elements
--theme.taglist_shape_volatile =

--The shape used for the volatile elements border width
--theme.taglist_shape_border_width_volatile =

--The volatile elements shape border color
--theme.taglist_shape_border_color_volatile =

--[[
  ___                          _           
 / __| ___ _ __  __ _ _ _ __ _| |_ ___ _ _ 
 \__ \/ -_) '_ \/ _` | '_/ _` |  _/ _ \ '_|
 |___/\___| .__/\__,_|_| \__,_|\__\___/_|  
          |_|                              
--]]

theme.mainseparator_thickness = dpi(2)
theme.mainseparator_color = theme.fg_focus.."ff"
theme.mainseparator_forced_width = dpi(15)
theme.mainseparator_top_margin = dpi(4)
theme.mainseparator_bottom_margin = dpi(4)
theme.mainseparator_orientation = "vertical"

--[[
   ___ _         _   
  / __| |___  __| |__
 | (__| / _ \/ _| / /
  \___|_\___/\__|_\_\
                     
--]]

theme.mainclock_bg = "#414141aa"
theme.mainclock_fg = theme.fg_focus
theme.mainclock_shape = gears.shape.rounded_rect

--[[
  _____ _                    ___                  
 |_   _| |_  ___ _ __  ___  / __|_ __ ____ _ _ __ 
   | | | ' \/ -_) '  \/ -_) \__ \ V  V / _` | '_ \
   |_| |_||_\___|_|_|_\___| |___/\_/\_/\__,_| .__/
                                            |_|   
--]]

theme.mainthemeswap_top_margin = dpi(4)
theme.mainthemeswap_bottom_margin = dpi(4)

--[[
  _  __                         
 | |/ /___ _  _ _ __  __ _ _ __ 
 | ' </ -_) || | '  \/ _` | '_ \
 |_|\_\___|\_, |_|_|_\__,_| .__/
           |__/           |_|   
--]]

theme.mainkeymap_top_margin = dpi(3)
theme.mainkeymap_bottom_margin = dpi(4)

--[[
  __  __          _ _              ___           __ 
 |  \/  |___ _ _ (_) |_ ___ _ _   / __|___ _ _  / _|
 | |\/| / _ \ ' \| |  _/ _ \ '_| | (__/ _ \ ' \|  _|
 |_|  |_\___/_||_|_|\__\___/_|    \___\___/_||_|_|  
                                                    
--]]

theme.monitorconf_top_margin = dpi(4)
theme.monitorconf_bottom_margin = dpi(4)

--[[
              _     _ _ _   _                   _    _____                          
     /\      | |   | (_) | (_)                 | |  / ____|                         
    /  \   __| | __| |_| |_ _  ___  _ __   __ _| | | (___   ___ _ __ ___  ___ _ __  
   / /\ \ / _` |/ _` | | __| |/ _ \| '_ \ / _` | |  \___ \ / __| '__/ _ \/ _ \ '_ \ 
  / ____ \ (_| | (_| | | |_| | (_) | | | | (_| | |  ____) | (__| | |  __/  __/ | | |
 /_/    \_\__,_|\__,_|_|\__|_|\___/|_| |_|\__,_|_| |_____/ \___|_|  \___|\___|_| |_|
                                                                                    
--]]

theme.basicpanel_bg = theme.bg_normal.."ff"
theme.basicpanel_fg = theme.fg_focus.."ff"
--TODO generic height
theme.basicpanel_height = dpi(20)
theme.basicpanel_ontop = false
theme.basicpanel_shape = gears.shape.rectangle

theme.basicpanel_pos = "top"
theme.basicpanel_hlayout = wibox.layout.fixed.horizontal
theme.basicpanel_vlayout = wibox.layout.fixed.vertical

--[[
  _                             _
 | |                           | |
 | |     __ _ _   _  ___  _   _| |_
 | |    / _` | | | |/ _ \| | | | __|
 | |___| (_| | |_| | (_) | |_| | |_
 |______\__,_|\__, |\___/ \__,_|\__|
               __/ |
              |___/
--]]

theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

--[[
   _____ _ _            _   
  / ____| (_)          | |  
 | |    | |_  ___ _ __ | |_ 
 | |    | | |/ _ \ '_ \| __|
 | |____| | |  __/ | | | |_ 
  \_____|_|_|\___|_| |_|\__|
                            
--]]

theme.useless_gap   = dpi(10)
theme.border_width  = dpi(4)
theme.border_normal = theme.transparent
--theme.border_focus  = "#535d6c"
theme.border_focus  = theme.fg_focus.."ff"
theme.border_marked = theme.fg_focus.."ff"
theme.client_shape = gears.shape.rounded_rect

--[[
  _____ _ _   _     _              
 |_   _(_) |_| |___| |__  __ _ _ _ 
   | | | |  _| / -_) '_ \/ _` | '_|
   |_| |_|\__|_\___|_.__/\__,_|_|  
                                   
--]]

theme.titlebar_fg = theme.mainpanel_fg

theme.titlebar_bg = theme.bg_focus.."88"

--theme.titlebar_fg_focus =

--theme.titlebar_bg_focus =

theme.titlebar_close_button_normal = icons.close

theme.titlebar_close_button_normal_hover = icons.close

theme.titlebar_close_button_normal_press = icons.close

theme.titlebar_close_button_focus = icons.close

theme.titlebar_close_button_focus_hover = icons.close

theme.titlebar_close_button_focus_press = icons.close


theme.titlebar_maximized_button_normal = icons.max

theme.titlebar_maximized_button_normal_active = icons.max

theme.titlebar_maximized_button_normal_active_hover = icons.max

theme.titlebar_maximized_button_normal_active_press = icons.max

theme.titlebar_maximized_button_normal_inactive = icons.max

theme.titlebar_maximized_button_normal_inactive_hover = icons.max

theme.titlebar_maximized_button_normal_inactive_press = icons.max

theme.titlebar_maximized_button_focus = icons.max

theme.titlebar_maximized_button_focus_active = icons.max

theme.titlebar_maximized_button_focus_active_hover = icons.max

theme.titlebar_maximized_button_focus_active_press = icons.max

theme.titlebar_maximized_button_focus_inactive = icons.max

theme.titlebar_maximized_button_focus_inactive_hover = icons.max

theme.titlebar_maximized_button_focus_inactive_press = icons.max


theme.titlebar_minimize_button_normal = icons.min

theme.titlebar_minimize_button_normal_hover = icons.min

theme.titlebar_minimize_button_normal_press = icons.min

theme.titlebar_minimize_button_focus = icons.min

theme.titlebar_minimize_button_focus_hover = icons.min

theme.titlebar_minimize_button_focus_press = icons.min

--[[
--
--]]

return theme
