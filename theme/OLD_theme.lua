---------------------------
-- My awesome theme --
---------------------------

local gears = require('gears')
local theme_assets = require('beautiful.theme_assets')
local xresources = require('beautiful.xresources')
local dpi = xresources.apply_dpi

local gfs = require('gears.filesystem')
local themes_path = gfs.get_themes_dir()
local CONFIG_DIR = gfs.get_configuration_dir()
local mytheme_path = CONFIG_DIR.."theme/"

local theme = {}

--[[
  ____                 
 |  _ \                
 | |_) | __ _ ___  ___ 
 |  _ < / _` / __|/ _ \
 | |_) | (_| \__ \  __/
 |____/ \__,_|___/\___|
                       
--]]

-- "dark" or "light" (defaults to dark of course^^)
theme.select_theme_mode = "light"

theme.blur_strength = 20

--print(mytheme_path)
--print(themes_path)

--theme.font          = "sans 8"
theme.font          = "SF Pro Text Bold 10"

theme.bg_normal     = "#222222"
theme.bg_focus      = "#535d6c"
theme.bg_urgent     = "#b34700"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = dpi(3)
theme.border_width  = dpi(1)
theme.border_normal = "#000000"
theme.border_focus  = "#535d6c"
theme.border_marked = "#91231c"

theme.transparent   = "#00000000"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"
theme.hotkeys_bg = "#000000"

--[[
  __  __       _       _                
 |  \/  |     (_)     | |               
 | \  / | __ _ _ _ __ | |__   __ _ _ __ 
 | |\/| |/ _` | | '_ \| '_ \ / _` | '__|
 | |  | | (_| | | | | | |_) | (_| | |   
 |_|  |_|\__,_|_|_| |_|_.__/ \__,_|_|   
                                        
--]]

--theme.mainbar_bg = "#22222222"
theme.mainbar_bg = "#00000000"
theme.mainbar_fg = "#ffffff"
theme.mainbar_height = 20
--defaults to top
theme.mainbar_position = "top"

--[[
  ___       _   _                
 | _ ) __ _| |_| |_ ___ _ _ _  _ 
 | _ \/ _` |  _|  _/ -_) '_| || |
 |___/\__,_|\__|\__\___|_|  \_, |
                            |__/ 
--]]

-- the battery update interval in seconds
theme.mainbar_battery_update = 5
theme.mainbar_battery_bg = theme.transparent
theme.mainbar_battery_fg = theme.mainbar_fg

theme.mainbar_battery_tooltip_enable = true
theme.mainbar_battery_tooltip_bg = theme.mainbar_bg



theme.mainbar_clock_bg = "#00000050"
theme.mainbar_clock_fg = theme.mainbar_fg
theme.mainbar_clock_shape = gears.shape.rounded_rect
theme.mainbar_clock_width = 50
theme.mainbar_clock_h_margin = 5

--[[
  _______          _                
 |__   __|        | |               
    | | __ _  __ _| |__   __ _ _ __ 
    | |/ _` |/ _` | '_ \ / _` | '__|
    | | (_| | (_| | |_) | (_| | |   
    |_|\__,_|\__, |_.__/ \__,_|_|   
              __/ |                 
             |___/                  
--]]

theme.big_tagbar = true

theme.tagbar_width = 40
--TODO add support for other positions than 'left' and 'right'
theme.tagbar_position = "left"
theme.tagbar_shape = gears.shape.rounded_rect
theme.tagbar_border_width = 0
theme.tagbar_border_color = "#00000044"

--theme.taglist_bg_focus = "#38385066"
--theme.master_fill_policy = "master_width_factor"
--theme.master_width_factor = 0.3 

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_spacing = dpi(30)
theme.taglist_shape = gears.shape.rounded_rect

if theme.select_theme_mode == "light" then
    theme.taglist_bg_focus = "#630000AA"
else
    --theme.taglist_bg_focus = "#60000066"
    theme.taglist_bg_focus = "#a40000ee"
end

--theme.bg_occupied = "#222222"

--theme.taglist_squares_resize = true
--theme.taglist_shape = gears.shape.square
--theme.taglist_shape = gears.shape.circle

--[[
  __  __                  
 |  \/  |                 
 | \  / | ___ _ __  _   _ 
 | |\/| |/ _ \ '_ \| | | |
 | |  | |  __/ | | | |_| |
 |_|  |_|\___|_| |_|\__,_|
                          
--]]

-- Variables set for theming the default awesomewm generated menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]

theme.menu_bg = "#22222266" 
theme.menu_fg = "#ffffffff"
theme.menu_width = 400
theme.menu_border_color = theme.menu_bg
theme.menu_border_width = dpi(5)
theme.menu_shape = gears.shape.rounded_rect
theme.menu_spacing = 4

-- visualize the grid pattern
theme.menu_debug = false

theme.menu_widgets_bg = "#cccfff35"
theme.menu_widgets_bg_margin = 10

theme.menu_cpustatus_update = 2

--[[
  _   _       _   _  __ _           _   _             
 | \ | |     | | (_)/ _(_)         | | (_)            
 |  \| | ___ | |_ _| |_ _  ___ __ _| |_ _  ___  _ __  
 | . ` |/ _ \| __| |  _| |/ __/ _` | __| |/ _ \| '_ \ 
 | |\  | (_) | |_| | | | | (_| (_| | |_| | (_) | | | |
 |_| \_|\___/ \__|_|_| |_|\___\__,_|\__|_|\___/|_| |_|
                                                      
--]]

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

--[[
  _______ _ _   _      _                
 |__   __(_) | | |    | |               
    | |   _| |_| | ___| |__   __ _ _ __ 
    | |  | | __| |/ _ \ '_ \ / _` | '__|
    | |  | | |_| |  __/ |_) | (_| | |   
    |_|  |_|\__|_|\___|_.__/ \__,_|_|   
                                        
--]]

theme.titlebar_fg = "#ffffff"
--theme.titlebar_bg = theme.mainbar_bg
--theme.titlebar_bg = "#cccfff70"
theme.titlebar_bg = "#000000DD"


-- Define the image to load
theme.titlebar_close_button_normal = mytheme_path.."icons/titlebar/close_focus.svg"
theme.titlebar_close_button_focus  = mytheme_path.."icons/titlebar/close_focus.svg"

--theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
--theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

--theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
--theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
--theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
--theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

--theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
--theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
--theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
--theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

--theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
--theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
--theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
--theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"

--[[
 __          __   _ _                             
 \ \        / /  | | |                            
  \ \  /\  / /_ _| | |_ __   __ _ _ __   ___ _ __ 
   \ \/  \/ / _` | | | '_ \ / _` | '_ \ / _ \ '__|
    \  /\  / (_| | | | |_) | (_| | |_) |  __/ |   
     \/  \/ \__,_|_|_| .__/ \__,_| .__/ \___|_|   
                     | |         | |              
                     |_|         |_|              
--]]

theme.wallpaper_dark = CONFIG_DIR.."theme/wallpapers/wallpaper_linux-dark.png"
theme.wallpaper_light = CONFIG_DIR.."theme/wallpapers/wallpaper_minimal-park.jpg"

--theme.wallpaper = themes_path.."default/background.png"
if theme.select_theme_mode == "light" then
    theme.wallpaper = theme.wallpaper_light
else
    theme.wallpaper = theme.wallpaper_dark
end


--[[
  _                             _   
 | |                           | |  
 | |     __ _ _   _  ___  _   _| |_ 
 | |    / _` | | | |/ _ \| | | | __|
 | |___| (_| | |_| | (_) | |_| | |_ 
 |______\__,_|\__, |\___/ \__,_|\__|
               __/ |                
              |___/                 
--]]

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

-- Generate Awesome icon:
--[[
--WARNING: before uncommenting: set theme_menu_height to the
--default value; the current menu_height is used in a different
--context!
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)
--]]

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
