--[[
  _____   _____ 
 |  __ \ / ____|
 | |__) | |     
 |  _  /| |     
 | | \ \| |____ 
 |_|  \_\\_____|
                
 This file is the entry point of awesome WM and will be the first file that
 is called after the window manager is started.
--]]

pcall(require, "luarocks.loader")

local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")
require("awful.autofocus")

terminal = "xfce4-terminal"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

CONFIG_DIR = gears.filesystem.get_configuration_dir()

--[[
  _                 _   __  __         _      _        
 | |   ___  __ _ __| | |  \/  |___  __| |_  _| |___ ___
 | |__/ _ \/ _` / _` | | |\/| / _ \/ _` | || | / -_|_-<
 |____\___/\__,_\__,_| |_|  |_\___/\__,_|\_,_|_\___/__/
                                                       
--]]

beautiful.init(CONFIG_DIR.."theme/theme.lua")

require("core")
require("configuration")

--[[
  ___                   _  _              _ _ _           
 | __|_ _ _ _ ___ _ _  | || |__ _ _ _  __| | (_)_ _  __ _ 
 | _|| '_| '_/ _ \ '_| | __ / _` | ' \/ _` | | | ' \/ _` |
 |___|_| |_| \___/_|   |_||_\__,_|_||_\__,_|_|_|_||_\__, |
                                                    |___/ 
--]]
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
