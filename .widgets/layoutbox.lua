local awful = require('awful')
local wibox = require('wibox')

local lb = function(s)
	local box = wibox.widget {
		awful.widget.layoutbox(s),
		top = 3,
		bottom = 3,
		left = 3,
		right = 3,
		widget = wibox.container.margin,
	}
	return box
end

return lb

