local awful = require('awful')
local theme = require('beautiful')
local wibox = require('wibox')

local myclock = wibox.widget.textclock("%H:%M:%S", 1)

local clock_bg = wibox.widget {
	{
		myclock,
		visible = true,
		left = theme.mainbar_clock_h_margin,
		right = theme.mainbar_clock_h_margin,
		top = 3,
		bottom = 3,
		widget = wibox.container.margin
	},
	bg = theme.mainbar_clock_bg,
	fg = theme.mainbar_clock_fg,
	--don't remove property 'forced_width' (used in widgets.menu_button.lua)
	--forced_width = theme.mainbar_clock_width,
	shape = theme.mainbar_clock_shape,
	widget = wibox.container.background,
}

local mycalendar = awful.widget.calendar_popup.month({
	-- TODO: remove bg and get proper blur support
	--bg = "#000000ff",
})
mycalendar:attach( myclock, 'tm' )

return clock_bg
