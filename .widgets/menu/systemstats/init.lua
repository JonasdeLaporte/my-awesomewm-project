local cpu_status = require('widgets.menu.systemstats.cpu_status')
local drive_status = require('widgets.menu.systemstats.drive_status')
local gears = require('gears')
local icons = require('theme.icons')
local ram_status = require('widgets.menu.systemstats.ram_status')
local temp_status = require('widgets.menu.systemstats.temp_status')
local theme = require('beautiful')
local wibox = require('wibox')

local margin = theme.menu_widgets_bg_margin

local title = wibox.widget {
	markup = "<b>System status</b>",
	widget = wibox.widget.textbox,
}

local systemstats = wibox.widget {
	{
		{
			title,
			cpu_status,	
			temp_status,
			ram_status,
			drive_status,
			layout = wibox.layout.fixed.vertical,
		},
		left = margin,
		right = margin,
		top = margin,
		bottom = margin,
		widget = wibox.container.margin,
	},
	bg = theme.menu_widgets_bg,
	fg = theme.menu_fg,
	forced_width = theme.menu_width - 2*theme.mainbar_height - 2*4,
	--forced_height = 120,
	shape = gears.shape.rounded_rect,
	widget = wibox.container.background,
}

return systemstats
