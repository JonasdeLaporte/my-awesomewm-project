local awful = require('awful')
local gears = require('gears')
local icons = require('theme.icons')
local theme = require('beautiful')
local wibox = require('wibox')

local interval = theme.menu_cpustatus_update
local margin = theme.menu_widgets_bg_margin

local ssd_logo = wibox.widget {
		{
			image = icons.hdd,
			resize = true,
			forced_height = 20,
			forced_width = 20,
 	 		widget = wibox.widget.imagebox
		},
		left = margin,
		right = margin,
		top = margin,
		bottom = margin,
		widget = wibox.container.margin,
}

local progress = wibox.widget {
	color = theme.menu_fg,
	background_color = theme.menu_widgets_bg,
	max_value     = 1,
    value         = 0.1,
    paddings      = 1,
	shape 		  = gears.shape.rounded_rect,
    forced_height = 4,
	margins		  = {
	top = 8,
	bottom = 8,
	},
    --border_width  = 1,
    --border_color  = theme.border_color,
    widget        = wibox.widget.progressbar,
}

local ssd_status = wibox.widget {
		ssd_logo,
		{
			progress,
			left = margin,
			right = margin,
			top = margin,
			bottom = margin,
			widget = wibox.container.margin,
		},
		layout = wibox.layout.fixed.horizontal,
}

--[[
awful.widget.watch("echo 'updating cpu status'", interval, function()
	--local value = math.fmod((progress.value + 0.05), 1)
	local value = progress.value
	local handle = io.popen("awk '{u=$2+$4; t=$2+$4+$5; if (NR==1){u1=u; t1=t;} else print ($2+$4-u1) * 100 / (t-t1); }' <(grep 'cpu ' /proc/stat) <(sleep 1;grep 'cpu ' /proc/stat)")	
	value = tonumber(handle:read("*a"))
	print(value)
	progress.value = value
	handle:close()
end)
--]]

return ssd_status
