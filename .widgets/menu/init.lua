local awful = require('awful')
local btn_close = require('widgets.menu.menu_close_button')
local btn_open = require('widgets.menu.menu_open_button')
local connections = require('widgets.menu.connections')
local gears = require('gears')
local icons = require('theme.icons')
local naughty = require('naughty')
local powermenu = require('widgets.menu.powermenu')
local quicksettings = require('widgets.menu.quicksettings')
local theme = require('beautiful')
local screen = awful.screen.focused()
local systemstats = require('widgets.menu.systemstats')
local wibox = require('wibox')

-- the menu_grid defines the layout of the widgets
local menu_grid = wibox.widget {
    --orientation = "horizontal",
    homogeneous = false,
    --expand = false,
    forced_width = theme.menu_width,
    spacing = theme.menu_spacing,
    forced_height = screen.geometry.height,
    forced_num_cols = 3,
    layout = wibox.layout.grid,
}

--[[
   _____                  _             
  / ____|                (_)            
 | (___  _ __   __ _  ___ _ _ __   __ _ 
  \___ \| '_ \ / _` |/ __| | '_ \ / _` |
  ____) | |_) | (_| | (__| | | | | (_| |
 |_____/| .__/ \__,_|\___|_|_| |_|\__, |
        | |                        __/ |
        |_|                       |___/ 
--]]

local left_spacing = wibox.widget {
    forced_width = theme.mainbar_height,
    forced_height = theme.mainbar_height,
    visible = true,
    border_width = 2,
    border_color = "#00000000",
    bg = "#ffffff00",
    widget = wibox.container.background,
}

if theme.menu_debug then
	left_spacing.bg = "#ffffff"
	left_spacing.border_color = "#000000"
end

local quicksettings_spacing = wibox.widget {
	forced_width = theme.mainbar_height,
	forced_height = quicksettings.forced_height,
	bg = left_spacing.bg,
	visible = true,
	border_width = left_spacing.border_width,
	border_color = left_spacing.border_color,
	widget = wibox.container.background,
}

local powermenu_spacing = wibox.widget {
	forced_width = theme.mainbar_height,
	forced_height = powermenu.forced_height,
	bg = left_spacing.bg,
	visible = true,
	border_width = left_spacing.border_width,
	border_color = left_spacing.border_color,
	widget = wibox.container.background,
}

local systemstats_spacing = wibox.widget {
	forced_width = theme.mainbar_height,
	forced_height = systemstats.forced_height,
	bg = left_spacing.bg,
	visible = true,
	border_width = left_spacing.border_width,
	border_color = left_spacing.border_color,
	widget = wibox.container.background,
}

local connections_spacing = wibox.widget {
	forced_width = theme.mainbar_height,
	forced_height = connections.forced_height,
	bg = left_spacing.bg,
	visible = true,
	border_width = left_spacing.border_width,
	border_color = left_spacing.border_color,
	widget = wibox.container.background,
}

--[[
  __  __                  
 |  \/  |                 
 | \  / | ___ _ __  _   _ 
 | |\/| |/ _ \ '_ \| | | |
 | |  | |  __/ | | | |_| |
 |_|  |_|\___|_| |_|\__,_|
                          
--]]

--Header
menu_grid:add_widget_at(left_spacing, 1, 1, 1, 1)
menu_grid:add_widget_at(left_spacing, 1, 2, 1, 1)
menu_grid:add_widget_at(btn_close, 1, 3, 1, 1)
menu_grid:add_widget_at(left_spacing, 1, 3, 1, 1)

--Powermenu
menu_grid:add_widget_at(powermenu_spacing, 2, 1, 1, 1)
menu_grid:add_widget_at(powermenu, 2, 2, 1, 1)
menu_grid:add_widget_at(powermenu_spacing, 2, 3, 1, 1)

local max_row = 6
for row=3,max_row-1,1 do
    for col=1,3,1 do
        menu_grid:add_widget_at(left_spacing, row, col, 1, 1)
    end
end

--Quicksettings
menu_grid:add_widget_at(quicksettings_spacing, max_row, 1, 1, 1)
menu_grid:add_widget_at(quicksettings, max_row, 2, 1, 1)
menu_grid:add_widget_at(quicksettings_spacing, max_row, 3, 1, 1)

--TODO: find solution for generic generation (on different screen 258 may not
--be enough or too much)
--solution idea:
--  use 'wibox.layout.grid:get_next_empty()' of the wibox.layout.grid class
--  to iterate all available cells (assuming they don't expand to outside the
--  screen -> could try a awful.placement. no_offscreen combination)
for row=max_row+1, 9,1 do
    for col=1,3,1 do
        menu_grid:add_widget_at(left_spacing, row, col, 1, 1)
    end
end

menu_grid:add_widget_at(connections_spacing, 10, 1, 1, 1)
menu_grid:add_widget_at(connections, 10, 2, 1, 1)
menu_grid:add_widget_at(connections_spacing, 10, 3, 1, 1)

for row=11, 26,1 do
    for col=1,3,1 do
        menu_grid:add_widget_at(left_spacing, row, col, 1, 1)
    end
end

menu_grid:add_widget_at(systemstats_spacing, 27, 1, 1, 1)
menu_grid:add_widget_at(systemstats, 27, 2, 1, 1)
menu_grid:add_widget_at(systemstats_spacing, 27, 3, 1, 1)

for row=28, 29,1 do
    for col=1,3,1 do
        menu_grid:add_widget_at(left_spacing, row, col, 1, 1)
    end
end

local menu_popup = awful.popup {
    widget = menu_grid,
    --placement = awful.placement.maximize_vertically,
    placement = awful.placement.top_left,
    visible = false,
    maximum_height = screen.geometry.height,
    ontop = true,
    bg = theme.menu_bg,
    border_width = theme.menu_border_width,
    border_color = theme.menu_border_color,
    shape = theme.menu_shape,
}

local function update_widgets()
	--naughty.notify({text = "UPDATE"})
	awesome.emit_signal('widgets::brightness_update')
	awesome.emit_signal('widgets::sound_update')
	awesome.emit_signal('widgets::speaker_update')
end

--[[
   _____ _                   _     
  / ____(_)                 | |    
 | (___  _  __ _ _ __   __ _| |___ 
  \___ \| |/ _` | '_ \ / _` | / __|
  ____) | | (_| | | | | (_| | \__ \
 |_____/|_|\__, |_| |_|\__,_|_|___/
            __/ |                  
           |___/                   
--]]

btn_open:connect_signal('button::press', function()
	update_widgets()
	menu_popup.visible = not(menu_popup.visible)
end)
btn_close:connect_signal('button::press', function()
	menu_popup.visible = not(menu_popup.visible)
end)
powermenu:connect_signal('close_menu', function()
	menu_popup.visible = not(menu_popup.visible)	
end)


return btn_open
