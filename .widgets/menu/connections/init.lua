local theme = require('beautiful')
local wibox = require('wibox')

local margin = theme.menu_widgets_bg_margin

local widget_title = wibox.widget {
	markup = "<b>Connections</b>",
	widget = wibox.widget.textbox,
}

local widget_bg = wibox.widget {
	{
		{
			widget_title,
			layout = wibox.layout.fixed.vertical,
		},
		left = margin,
		right = margin,
		top = margin,
		bottom = margin,
		widget = wibox.container.margin,

	},
	fg = theme.menu_fg,
	bg = theme.menu_widgets_bg,
	shape = theme.menu_shape,
	--forced_width = 300,
	forced_height = 100,
	widget = wibox.container.background,
}

return widget_bg
