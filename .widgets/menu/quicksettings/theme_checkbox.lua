local gears = require('gears')
local theme = require('beautiful')
local wibox = require('wibox')

local flag = (theme.select_theme_mode == "dark")

local chk = wibox.widget {
    checked       = flag,
    color         = theme.bg_normal,
    paddings      = 2,
    shape         = gears.shape.circle,
    widget        = wibox.widget.checkbox
}

local check = wibox.widget {
	chk,
	forced_width = 20,
	forced_height = 20,
	widget = wibox.container.constraint,
}

chk:connect_signal('button::press', function()
	chk.checked = not(chk.checked)
	if theme.select_theme_mode == "light" then
		theme.select_theme_mode = "dark"
		--print("Changing theme to dark!")
	else	
		theme.select_theme_mode = "light"
		--print("Changing theme to light!")
	end
	--os.execute("kill $(pidof compton)")
	--awesome.restart()
end)

return check
