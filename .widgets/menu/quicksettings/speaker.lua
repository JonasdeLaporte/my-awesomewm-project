
local icons = require('theme.icons')
local naughty = require('naughty')
local sound = require('widgets.menu.quicksettings.slider_sound')
local wibox = require('wibox')

local muted = false

local speaker = wibox.widget {
	image = icons.speaker,
	forced_width = 20,
	forced_height = 20,
	widget = wibox.widget.imagebox,
}

speaker.update = function()
	if muted then
		muted = false
		os.execute('pactl set-sink-mute 0 0')
		speaker.image = icons.speaker
	else
		muted = true
		os.execute('pactl set-sink-mute 0 1')
		speaker.image = icons.mute
	end	
end

speaker.refresh = function()
	local handle = io.popen('pamixer --get-mute')
	local status = handle:read("*l")
	if status == "true" then
		speaker.image = icons.mute
	else
		speaker.image = icons.speaker
	end
	handle:close()
end

speaker:connect_signal('button::press', speaker.update)
awesome.connect_signal('widgets::speaker_update', speaker.refresh)

return speaker
