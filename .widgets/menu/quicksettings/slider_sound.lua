local awful = require('awful')
local gears = require('gears')
local naughty = require('naughty')
local theme = require('beautiful')
local wibox = require('wibox')

local sl = wibox.widget {
	--{
		id					= 'my_slider',
        bar_shape           = gears.shape.rounded_rect,
        bar_height          = 4,
        bar_color           = theme.menu_fg,
		handle_width		= 15,
        handle_color        = theme.menu_fg,
        handle_shape        = gears.shape.circle,
        handle_border_color = theme.menu_fg,
        handle_border_width = 1,
        value               = 10, 
        visible             = true,
		minimum				= 1,
		maximum				= 100,
        widget              = wibox.widget.slider,
    --},  
    --forced_width = 400 - 2*theme.mainbar_height - 2*4,
    --forced_height = 40,
	--top = 5,
	--bottom = 5,
    --widget = wibox.container.margin,
}


--[[
   _____ _                   _     
  / ____(_)                 | |    
 | (___  _  __ _ _ __   __ _| |___ 
  \___ \| |/ _` | '_ \ / _` | / __|
  ____) | | (_| | | | | (_| | \__ \
 |_____/|_|\__, |_| |_|\__,_|_|___/
            __/ |                  
           |___/                   
--]]

--local sldr = sl.my_slider
local sldr = sl
sldr:connect_signal('property::value', function()
	os.execute("bash -c 'pactl set-sink-volume 0 "..sldr:get_value().."% &'")
end)

-- backlight could be externally changed (e.g by cmd); the signal gets emitted
-- by widgets.menu.menu (on menu open)
awesome.connect_signal('widgets::sound_update', function ()
	local handle = io.popen("pamixer --get-volume")
	sldr:set_value(tonumber(handle:read("*a")))
	handle:close()
end)

--[[
  ____        _   _                  
 |  _ \      | | | |                 
 | |_) |_   _| |_| |_ ___  _ __  ___ 
 |  _ <| | | | __| __/ _ \| '_ \/ __|
 | |_) | |_| | |_| || (_) | | | \__ \
 |____/ \__,_|\__|\__\___/|_| |_|___/
                                     
--]]

-- add support for scrolling
sl:buttons(
	gears.table.join(
		awful.button(
			{},
			4,
			nil,
			function ()
				--naughty.notify({text = "slider inc"})
				local level = sldr:get_value()
				if level < 100 then
					sldr:set_value(level+5)
				end
			end
		),
		awful.button(
			{},
			5,
			nil,
			function ()
				--naughty.notify({text = "slider dec"})
				local level = sldr:get_value()
				if 0 < level then
					sldr:set_value(level-5)
				end
			end
		)
	)
)

return sl
