--local check = require('widgets.menu.theme_checkbox')
local icons = require('theme.icons')
local brightness = require('widgets.menu.quicksettings.slider_brightness')
local sound = require('widgets.menu.quicksettings.slider_sound')
local speaker = require('widgets.menu.quicksettings.speaker')
local theme = require('beautiful')
local wibox = require('wibox')

local margin = theme.menu_widgets_bg_margin

brightness.forced_height = 20
sound.forced_height = 20

local spacing = wibox.widget {
	bg = theme.transparent,
	forced_height = margin,
	widget = wibox.container.background,
}

local widget_bg = wibox.widget {
		{
			{
				-- add widgets here; order matters
				{	
					markup = "<b>Quicksettings</b>",
					widget = wibox.widget.textbox,	
				},
				spacing,
				-- brightness widget
				{
					{
						{
							image = icons.sun,
							forced_width = brightness.forced_height,
							forced_height = brightness.forced_height,
							widget = wibox.widget.imagebox,
						},
						right = margin,
						widget = wibox.container.margin,
					},
					brightness,
					layout = wibox.layout.align.horizontal,
				},
				spacing,
				-- sound widget
				{
					{
						speaker,
						right = margin,
						widget = wibox.container.margin,
					},
					sound,
					layout = wibox.layout.align.horizontal,
				},
				layout = wibox.layout.fixed.vertical,
			},
			left = margin,
			right = margin,
			top = margin,
			bottom = margin,
			widget = wibox.container.margin,
		},
		fg = theme.menu_fg,
		bg = theme.menu_widgets_bg,	
		forced_width = theme.menu_width - 2*theme.mainbar_height - 2*4,
		shape = theme.menu_shape,
		widget = wibox.container.background,
}

return widget_bg
