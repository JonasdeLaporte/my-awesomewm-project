local icons = require('theme.icons')
local naughty = require('naughty')
local theme = require('beautiful')
local wibox = require('wibox')

local power = wibox.widget {
	{
		image = icons.power,
		widget = wibox.widget.imagebox,
	},
	top = 10,
	bottom = 10,
	left = 10,
	right = 10,
	widget = wibox.container.margin,
}

local sleep = wibox.widget {
	{
		image = icons.sleep,
		widget = wibox.widget.imagebox,
	},
	top = 10,
	bottom = 10,
	left = 10,
	right = 10,
	widget = wibox.container.margin,
}

local reboot = wibox.widget {
	{
		image = icons.reboot,
		widget = wibox.widget.imagebox,
	},
	top = 10,
	bottom = 10,
	left = 10,
	right = 10,
	widget = wibox.container.margin,
}

local logout = wibox.widget {
	{
		image = icons.logout,
		widget = wibox.widget.imagebox,
	},
	top = 10,
	bottom = 10,
	left = 10,
	right = 10,
	widget = wibox.container.margin,
}

local spacing = wibox.widget {
	bg = theme.transparent,
	forced_height = 50,
	forced_width = 75,
	widget = wibox.container.background,
}

local powermenu = wibox.widget {
	{
		spacing,
		power,
		sleep,
		reboot,
		logout,
		layout = wibox.layout.fixed.horizontal,
	},
	bg = theme.menu_widgets_bg,
	forced_height = 50,
	forced_width = theme.menu_width - 2*theme.mainbar_height - 2*4,
	shape = theme.menu_shape,
	widget = wibox.container.background,
}

local function close_menu()
	--naughty.notify({text = "closing signal emitted"})
	powermenu:emit_signal("close_menu")
end

power:connect_signal('button::press', function ()
	--naughty.notify({text = "Poweroff"})
	close_menu()
	os.execute("poweroff")
end)

sleep:connect_signal('button::press', function ()
	--naughty.notify({text = "Sleep"})
	close_menu()
	os.execute("systemctl suspend")
end)

reboot:connect_signal('button::press', function ()
	--naughty.notify({text = "Reboot"})
	close_menu()
	os.execute("reboot")
end)

logout:connect_signal('button::press', function ()
	--naughty.notify({text = "Logout"})
	close_menu()
	awesome.quit()
end)

return powermenu
