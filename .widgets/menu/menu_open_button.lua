local awful = require('awful')
local gears = require('gears')
local icons = require('theme.icons')
local theme = require('beautiful')
local screen = awful.screen.focused()
local wibox = require('wibox')

local opener = icons.menu_darkmode

--[[
if theme.select_theme_mode == "light" then
	opener = icons.menu_lightmode
else
	opener = icons.menu_darkmode
end
--]]

local btn_image = wibox.widget {
	image = opener,
	resize = true,
	widget = wibox.widget.imagebox,
}

-- restrict width of the logo widget to prevent the button from taking the
-- whole space after adding it to the grid-based menu
local btn = wibox.widget {
	{
		btn_image,
		forced_height = theme.mainbar_height,
		forced_width = theme.mainbar_height,
		widget = wibox.container.constraint,
	},
	left = 6,	
	top = 2,
	bottom = 2,
	widget = wibox.container.margin,
}

local b_width = 50
if theme.big_tagbar then
	b_width = screen.geometry.width/2 - theme.mainbar_clock_h_margin*2
end

local open_button = wibox.widget {
	btn,
	--bg = "#ff0000",
	visible = true,
	shape = gears.shape.rectangle,
	-- keep clock centered by stretching the background of the button in the
	-- direction of the clock
	forced_width = b_width,
	widget = wibox.container.background,
}

return open_button
