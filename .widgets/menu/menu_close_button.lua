local gears = require('gears')
local icons = require('theme.icons')
local theme = require('beautiful')
local wibox = require('wibox')



local btn_image = wibox.widget {
		image = icons.close_darkmode,
		resize = true,
		widget = wibox.widget.imagebox,
}

local btn = wibox.widget {
	--{
		btn_image,
		forced_height = theme.mainbar_height,
		forced_width = theme.mainbar_height,
		widget = wibox.container.constraint,
	--},
	--right = 60,
	--top = 30,
	--widget = wibox.container.margin,
}

local close_button = wibox.widget {
	btn_image,
	--bg = "#ff0000",
	top = 3,
	bottom = 3,
	left = 3,
	right = 3,
	forced_height = theme.mainbar_height,
	forced_width = theme.mainbar_height,
	--shape = gears.shape.rectangle, 
	--widget = wibox.container.background,
	widget = wibox.container.margin,
}

return close_button
