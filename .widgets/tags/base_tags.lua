local awful = require('awful')
local icons = require('theme.icons')
local l = awful.layout.suit

return {
	{
        name = "0",
        icon = icons.document,
        layout = l.fair,
    },
}
