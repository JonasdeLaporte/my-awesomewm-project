local awful = require('awful')
local icons = require('theme.icons')
local naughty = require('naughty')
local l = awful.layout.suit

local tags = {
	{
        name = "1",
        icon = icons.one_alt,
        layout = l.floating,
    },
    {
        name = "2",
        icon = icons.two_alt,
        layout = l.fair,
    },
    {
        name = "3",
        icon = icons.three_alt,
        layout = l.fair,
    },
    {
        name = "4",
        icon = icons.four_alt,
        layout = l.fair,
    },
    {
        name = "5",
        icon = icons.five_alt,
        layout = l.fair,
    },
    {
        name = "6",
        icon = icons.six_alt,
        layout = l.fair,
    },  
    {   
        name = "7",
        icon = icons.seven_alt,
        layout = l.fair,
    },  
    {
        name = "8",
        icon = icons.eight_alt,
        layout = l.fair,
    },
    {
        name = "9",
        icon = icons.nine_alt,
        layout = l.fair,
    },

}

return tags
