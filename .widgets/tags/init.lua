local awful = require('awful')
local beautiful = require('beautiful')
local naughty = require('naughty')
local tags = {}

local als = awful.layout.suit

screen.connect_signal('request::desktop_decoration', function(s)
	if s.index == 1 then
		tags = require('widgets.tags.main_tags')
	else
		tags = require('widgets.tags.base_tags')
	end
	
	for i,tag in ipairs(tags) do
        local t = awful.tag.add(tag.name, {
            screen = s,
            index = i,
            layout = tag.layout,
            gap_single_client = true,
            gap = beautiful.useless_gap,
            selected = i == 1,
            volatile = false,
            --layouts = awful.layout.layouts,
            -- gap = x,
            master_width_factor = 0.1,
            master_fill_policy = "master_width_factor",
            icon = tag.icon,
        })
    end
	--on_layout_request(tag)
end)

--[[
--
--TODO: limit available layouts to floating, fairv, max
--
tag.connect_signal('request::default_layouts', function(s)
		naughty.notify({text = 'setting default layouts for screen '..s})
		awful.layout.append_default_layout({
        	als.floating,
        	--als.tile,
        	--als.tile.left,
        	--als.tile.bottom,
        	--als.tile.top,
        	--als.fair,
        	als.fair.horizontal,
        	--als.spiral,
        	--als.spiral.dwindle,
        	als.max,
        	als.max.fullscreen,
        	--als.magnifier,
        	--als.corner.nw,
        	-- als.corner.ne
        	-- als.corner.sw
     	    -- als.corner.se
    	})
end)
--]]
