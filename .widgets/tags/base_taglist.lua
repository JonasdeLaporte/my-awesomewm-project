local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')


-- add mouse functionality
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end)
				)

local mytaglist = awful.widget.taglist({
	id = "list",
	screen = 2,
	filter = awful.widget.taglist.filter.all,
	buttons = taglist_buttons,
	layout = wibox.layout.fixed.vertical
})

local tagwidget = wibox.widget {
	mytaglist,
	placement = awful.placement.centered,
	left = 3,
	right = 3,
	widget = wibox.container.margin,
}
return tagwidget
