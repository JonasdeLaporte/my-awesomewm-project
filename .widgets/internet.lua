local awful = require('awful')
local icons = require('theme.icons')
local naughty = require('naughty')
local theme = require('beautiful')
local wibox = require('wibox')

local ip = wibox.widget {
	text = "  IP ADRESS  ",
	visible = false,
	widget = wibox.widget.textbox,
}

local function get_logo() 
	local logo = icons.wifi
	--local handle = io.popen("ip route get 8.8.4.4 | head -1 | awk '{print $7}'")

	--handle:close()
	return logo
end

local ip_text = wibox.widget {
	{
		ip,
		fg = theme.mainbar_fg,
		widget = wibox.container.background,
	},
	visible = false,
	left = 3,
	right = 3,
	widget = wibox.container.margin,
}

local internet = wibox.widget {
	{
		{
		image = get_logo(),
		resize = true,
		widget = wibox.widget.imagebox,
		},
		ip_text,
		layout = wibox.layout.align.horizontal,
	},
	left = 7,
	right = 7,
	top = 2,
	bottom = 2,
	widget = wibox.container.margin,
}

--[[
	naughty.notify({
		text = "WIFI gets called!",
	})
--]]

internet:connect_signal('button::press', function()
	local flag = ip.visible
	if not(flag) then
		local handle = io.popen("ip route get 8.8.4.4 | head -1 | awk '{print $7}'")
		ip.text = handle:read("*a")
		handle:close()
	end	
	ip.visible = not(flag)
	ip_text.visible = not(flag)
end)



return internet
