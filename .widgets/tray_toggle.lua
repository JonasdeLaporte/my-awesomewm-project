local icons = require('theme.icons')
local theme = require('beautiful')
local wibox = require('wibox')


local t_image = wibox.widget {
	image = icons.tray_closed,
	resize = true,
	widget = wibox.widget.imagebox,
}

local trays = wibox.widget {
	{
		id = 'systrays',
		visible = false,
		widget = wibox.widget.systray,
	},
	left = 3,
	right = 3,
	top = 1,
	bottom = 1,
	widget = wibox.container.margin,
}

theme.bg_systray = theme.main_bg
theme.systray_icon_spacing = 5

local tray_toggle = wibox.widget {
	{
		trays,
		{
			t_image,
			top = 3,
			bottom = 3,
			widget = wibox.container.margin,
		},
		layout = wibox.layout.fixed.horizontal,
	},
	bg = theme.transparent,
	fg = theme.mainbar_fg,
	widget = wibox.container.background,
}

tray_toggle:connect_signal('button::press', function()
	if trays.systrays.visible then
		t_image.image = icons.tray_closed
	else
		t_image.image = icons.tray_opened
	end
	trays.systrays.visible = not(trays.systrays.visible)
end)

return tray_toggle
